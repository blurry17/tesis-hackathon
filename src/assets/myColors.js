export const myColors = {
    primary: "#004178",
    green: "#3CB371",
    red: "#CD5C5C",
    dark: "#1a202a",
    white: "#ffffff",
    darkGray: "#333333",
    lightGray: "#90A0B7",
    littleGray: "#E7E7E7",
    black: '#000000',
    skyBlue: '#3085d6'
};
