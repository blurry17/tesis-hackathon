import React from 'react';
import { useStyles } from './Dropzone.module';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import { Paper } from '@material-ui/core';

const Dropzone = (props) => {
    const classes = useStyles();
    const fileInputRef = React.createRef();
    const { disabled, multiple, image } = props;

    const openFileDialog = () => {
        if (disabled) return;
        fileInputRef.current.click();
    }

    const fileListToArray = (list) => {
        const array = [];
        for (var i = 0; i < list.length; i++) {
            array.push(list.item(i));
        }
        return array;
    }

    const onFilesAdded = (e) => {
        if (disabled) return;
        const files = e.target.files;
        if (props.onFilesAdded) {
            const array = fileListToArray(files);
            props.onFilesAdded(array);
        }
    }

    const onDragOver = (e) => {
        e.preventDefault();
        if (disabled) return;
    }

    const onDrop = (e) => {
        e.preventDefault();
        if (disabled) return;
        const files = e.dataTransfer.files;
        if (props.onFilesAdded) {
            const array = fileListToArray(files);
            props.onFilesAdded(array);
        }
    }

    return (
        <Paper className={classes.dropzone}
            onDragOver={onDragOver}
            onDrop={onDrop}
            onClick={openFileDialog}
            style={{ cursor: disabled ? "default" : "pointer" }}
        >
            <CloudUploadIcon className={classes.icon} />
            <input
                ref={fileInputRef}
                className={classes.fileInput}
                type="file"
                accept={image ? "image/*" : "*"}
                multiple={multiple}
                onChange={onFilesAdded}
            />
            <span>Arrastre sus archivos aquí o haga click para buscar</span>
        </Paper>
    );
};

export default Dropzone;