import React, { useState } from 'react';
import Alert from '@material-ui/lab/Alert';
import { useStyles } from './UploadFiles.module';
import Dropzone from './Dropzone';

const UploadFiles = (props) => {
    const classes = useStyles();
    const uploading = false;
    const successfullUploaded = false;
    const { files, setFiles, multiple, type } = props;
    //const [files, setFiles] = useState([])

    const onFilesAdded = (fileArray) => {
        let encontrado = false;


        if (files === []) {
            setFiles(prevFiles => files.concat(fileArray));
        } else {
            files.map((x) => {
                if (x.name === fileArray[0].name) encontrado = true;
                return x;
            })
            if (!encontrado) {
                setFiles(prevFiles => files.concat(fileArray));
            }
        }
    }

    const formatBytes = (bytes, decimals = 2) => {
        if (bytes === 0) return '0 Bytes';

        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        const i = Math.floor(Math.log(bytes) / Math.log(k));

        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    const eliminarArchivo = (data) => {
        let aux = [...files]
        aux.map((x, index) => {
            if (x.name === data) aux.splice(index, 1);
            return x;
        })
        setFiles(aux)
    }

    return (
        <div className={classes.upload}>
            <Dropzone
                onFilesAdded={onFilesAdded}
                disabled={uploading || successfullUploaded}
                multiple={multiple}
            />
            <div className={classes.files}>
                {files.map(file => {
                    return (
                        <Alert key={file.name}
                            onClose={() => eliminarArchivo(file.name)}>{file.name} - {formatBytes(file.size)}</Alert>
                    )
                })}
            </div>
        </div>
    );
};

export default UploadFiles;
