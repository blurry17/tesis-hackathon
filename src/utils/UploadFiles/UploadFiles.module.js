import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
    files: {
        marginTop: 10,
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(1),
        },
    },
}));