import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
    dropzone: {
        //paddingTop: 20,
        //paddingBottom: 20,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        fontSize: 16,
        border: `2px dashed black`,
    },
    fileInput: {
        display: "none",
    },
    icon: {
        height: 64,
        width: 64,
        color: 'primary',
    }
}));