import React, { useState, createContext } from "react";

export const UserContext = createContext();

export const MainProvider = (props) => {
    const [user, setUser] = useState(null);

    const obj = {
        user,
        setUser
    }

    return (<UserContext.Provider value={obj}>{props.children}</UserContext.Provider>);
};
