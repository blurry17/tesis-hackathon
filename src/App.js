import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import TopBarContainer from './containers/TopBarContainer';
import MainPageContainer from './containers/MainPageContainer';
import MyEventsContainer from './containers/MyEventsContainer';
import AfterRegister from './components/AfterRegister/AfterRegister'
import CreateEvent from './components/CreateEvent/CreateEvent';
import EventInfo from './components/EventInfo/EventInfo';
import Register from './components/Register/Register'
import Alerts from './components/Alerts/Alerts'
import Login from './components/Login/Login'
import Teams from './components/Teams/Teams'
import After from './components/After/After';
import During from './components/During/During';
import Before from './components/Before/Before';
import Hacking from './components/Hacking/Hacking';
import Profile from './components/Profile/Profile';

var logged = localStorage.getItem('accessToken') !== null

function App() {
  return (
    <>
      {logged ? (
        <Router>
          <TopBarContainer />
          <Switch>
            <Route exact path="/" component={MainPageContainer} />
            <Route exact path="/d" component={MyEventsContainer} />
            <Route exact path="/create" component={CreateEvent} />
            <Route exact path="/myTeams" component={Teams} />
            <Route exact path="/profile" component={Profile} />
            <Route exact path="/alerts*" component={Alerts} />
            <Route exact path="/event/*" component={EventInfo} />
            <Route path="/before/:id" component={Before} />
            <Route exact path="/during/:id" component={During} />
            <Route exact path="/after/:id" component={After} />
            <Route exact path="/hack/:id" component={Hacking} />
            <Route path="*" render={() => <Redirect to="/" />} />
          </Switch>
        </Router>
      ) : (
          <Router>
            <TopBarContainer />
            <Switch>
              <Route exact path="/" component={MainPageContainer} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/afterRegister" component={AfterRegister} />
              <Route exact path="/event/*" component={EventInfo} />
              <Route path="*" render={() => <Redirect to="/login" />} />
            </Switch>
          </Router>
        )}
    </>
  )
}
export default App;