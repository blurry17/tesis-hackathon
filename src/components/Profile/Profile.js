import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { useStyles } from './Profile.module'
import { Auth } from 'aws-amplify';
import { UserGetBasic, UserGetSchools, UserGetMajors, UserGetGenders, UserGetLevels, UserGetDiets, UserEdit } from '../../api/services';
import { Snackbar, Select, MenuItem, InputLabel, FormControl } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';

export default function Profile() {
    const classes = useStyles();
    const [openSnack, setOpenSnack] = useState(false);
    const [severitySnack, setSeveritySnack] = useState('success');
    const [messageSnack, setMessageSnack] = useState('');
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [phone, setPhone] = useState('')
    const [bday_day, setBday_day] = useState('')
    const [bday_month, setBday_month] = useState('')
    const [bday_year, setBday_year] = useState('')
    const [needs, setNeeds] = useState('')
    const [description, setDescription] = useState('')
    const [gender, setGender] = useState(0)
    const [school, setSchool] = useState(0);
    const [major, setMajor] = useState(0);
    const [diet, setDiet] = useState(0);
    const [studylvl, setStudylvl] = useState(0);
    const [schools, setSchools] = useState([]);
    const [majors, setMajors] = useState([]);
    const [genders, setGenders] = useState([]);
    const [diets, setDiets] = useState([]);
    const [levels, setLevels] = useState([]);

    const popSnack = (msg, sev) => {
        setMessageSnack(msg)
        setSeveritySnack(sev)
        setOpenSnack(true)
    }

    const handleClickUpdate = async () => {
        let user = await Auth.currentUserInfo();
        let data = {
            iduser: Number(user.attributes["custom:iduser"]),
            description: description,
            bday_day: bday_day,
            bday_month: bday_month,
            bday_year: bday_year,
            gender: gender,
            school: school,
            major: major,
            studylvl: studylvl,
            diet: diet,
            needs: needs,
            phone: phone
        }
        let t = await UserEdit(data)
        if (t.body !== null) popSnack('Datos actualizados', 'success')
    }

    const getUserInfo = async () => {
        let user = await Auth.currentUserInfo();
        let t = await UserGetBasic({ iduser: Number(user.attributes["custom:iduser"]) })
        setName(t.body.name + ' ' + t.body.lastName);
        setEmail(t.body.email)
        setDescription(t.body.description);
        setBday_day(t.body.bday_day);
        setBday_month(t.body.bday_month);
        setBday_year(t.body.bday_year);
        setGender(t.body.gender);
        setSchool(t.body.school);
        setMajor(t.body.major);
        setNeeds(t.body.special_needs);
        setPhone(t.body.phone);
        setDiet(t.body.diet_pref);
        setStudylvl(t.body.study_lvl);
    }

    const getMajors = async () => { let t = await UserGetMajors(); setMajors(t.body) }
    const getGenders = async () => { let t = await UserGetGenders(); setGenders(t.body) }
    const getSchools = async () => { let t = await UserGetSchools(); setSchools(t.body) }
    const getLevels = async () => { let t = await UserGetLevels(); setLevels(t.body) }
    const getDiets = async () => { let t = await UserGetDiets(); setDiets(t.body) }

    useEffect(() => {
        getUserInfo();
        getMajors();
        getGenders();
        getSchools();
        getLevels();
        getDiets();
    }, []);

    return (
        <>
            <Container component="main" maxWidth="sm">
                <CssBaseline />
                <div className={classes.paper}>
                    <Typography component="h1" variant="h5">
                        Mi perfil
                </Typography>
                    <form className={classes.form} noValidate>
                        <Grid container spacing={1}>
                            <Grid item sm={12}>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    disabled
                                    fullWidth
                                    id="name"
                                    label="Nombre"
                                    name="name"
                                    value={name}
                                    onChange={(e) => setName(e.target.value)}
                                    InputLabelProps={{ shrink: true }}
                                />
                            </Grid>
                            <Grid item sm={6}>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    disabled
                                    fullWidth
                                    id="email"
                                    label="Correo electrónico"
                                    name="email"
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                    InputLabelProps={{ shrink: true }}
                                />
                            </Grid>
                            <Grid item sm={6}>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    label="Teléfono"
                                    value={phone}
                                    onChange={(e) => setPhone(e.target.value)}
                                    InputLabelProps={{ shrink: true }}
                                />
                            </Grid>
                            <Grid item sm={12}>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    label="Descripción"
                                    value={description}
                                    onChange={(e) => setDescription(e.target.value)}
                                    InputLabelProps={{ shrink: true }}
                                    multiline
                                    rowsMax={4}
                                />
                            </Grid>
                            <Grid item sm={6}>
                                <FormControl variant="outlined" className={classes.formControl} fullWidth>
                                    <InputLabel id="equipo-select" shrink={true}>Universidad/Instituto</InputLabel>
                                    <Select
                                        value={school}
                                        onChange={(e) => setSchool(e.target.value)}
                                        label="Universidad/Instituto"
                                        fullWidth
                                    >
                                        <MenuItem value={0}>
                                            <em>Seleccionar</em>
                                        </MenuItem>
                                        {(schools.map((t) => {
                                            return (
                                                <MenuItem key={t.idschool} value={t.idschool}>{t.name}</MenuItem>
                                            )
                                        }))}
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item sm={6}>
                                <FormControl variant="outlined" className={classes.formControl} fullWidth>
                                    <InputLabel id="equipo-select" shrink={true}>Carrera</InputLabel>
                                    <Select
                                        value={major}
                                        onChange={(e) => setMajor(e.target.value)}
                                        label="Carrera"
                                        fullWidth
                                    >
                                        <MenuItem value={0}>
                                            <em>Seleccionar</em>
                                        </MenuItem>
                                        {(majors.map((t) => {
                                            return (
                                                <MenuItem key={t.idmajor} value={t.idmajor}>{t.name}</MenuItem>
                                            )
                                        }))}
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item sm={6}>
                                <FormControl variant="outlined" className={classes.formControl} fullWidth>
                                    <InputLabel id="equipo-select" shrink={true}>Nivel de estudios</InputLabel>
                                    <Select
                                        value={studylvl}
                                        onChange={(e) => setStudylvl(e.target.value)}
                                        label="Nivel de estudios"
                                    >
                                        <MenuItem value={0}>
                                            <em>Seleccionar</em>
                                        </MenuItem>
                                        {(levels.map((t) => {
                                            return (
                                                <MenuItem key={t.idstudy_lvl} value={t.idstudy_lvl}>{t.name}</MenuItem>
                                            )
                                        }))}
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item sm={6}>
                                <FormControl variant="outlined" className={classes.formControl} fullWidth>
                                    <InputLabel id="equipo-select" shrink={true}>Género</InputLabel>
                                    <Select
                                        value={gender}
                                        onChange={(e) => setGender(e.target.value)}
                                        label="Carrera"
                                    >
                                        <MenuItem value={0}>
                                            <em>Seleccionar</em>
                                        </MenuItem>
                                        {(genders.map((t) => {
                                            return (
                                                <MenuItem key={t.idgender} value={t.idgender}>{t.name}</MenuItem>
                                            )
                                        }))}
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item sm={4}>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    label="Día"
                                    value={bday_day}
                                    onChange={(e) => setBday_day(e.target.value)}
                                    InputLabelProps={{ shrink: true }}
                                />
                            </Grid>
                            <Grid item sm={4}>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    label="Mes"
                                    value={bday_month}
                                    onChange={(e) => setBday_month(e.target.value)}
                                    InputLabelProps={{ shrink: true }}
                                />
                            </Grid>
                            <Grid item sm={4}>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    label="Año"
                                    value={bday_year}
                                    onChange={(e) => setBday_year(e.target.value)}
                                    InputLabelProps={{ shrink: true }}
                                />
                            </Grid>
                            <Grid item sm={12}>
                                <FormControl variant="outlined" className={classes.formControl} fullWidth>
                                    <InputLabel id="equipo-select" shrink={true}>Preferencia alimenticia</InputLabel>
                                    <Select
                                        value={diet}
                                        onChange={(e) => setDiet(e.target.value)}
                                        label="Preferencia alimenticia"
                                    >
                                        <MenuItem value={0}>
                                            <em>Seleccionar</em>
                                        </MenuItem>
                                        {(diets.map((t) => {
                                            return (
                                                <MenuItem key={t.iddiet_pref} value={t.iddiet_pref}>{t.name}</MenuItem>
                                            )
                                        }))}
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item sm={12}>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    label="Necesidades especiales"
                                    value={needs}
                                    onChange={(e) => setNeeds(e.target.value)}
                                    InputLabelProps={{ shrink: true }}
                                />
                            </Grid>
                        </Grid>
                        <Button
                            fullWidth
                            variant="contained"
                            color="primary"
                            onClick={handleClickUpdate}
                            className={classes.submit}
                            submit
                        >
                            Guardar
                        </Button>
                    </form>
                </div>
            </Container>
            <Snackbar open={openSnack} autoHideDuration={3000} onClose={() => setOpenSnack(false)}>
                <Alert onClose={() => setOpenSnack(false)} severity={severitySnack}>
                    {messageSnack}
                </Alert>
            </Snackbar>
        </>
    );
}
