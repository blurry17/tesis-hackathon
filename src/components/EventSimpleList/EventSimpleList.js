import React from 'react';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from "react-router-dom";
import { EventGetTeamId, EventGetRoleId, EventGetTeamMentoring } from '../../api/services';
import { Auth } from 'aws-amplify';

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
    },
    media: {
        height: '5rem',
    },
});

const EventSimpleList = (props) => {
    const { title, events, mode } = props;
    const classes = useStyles();
    const history = useHistory();

    const handleClickBefore = async (event) => {
        let user = await Auth.currentUserInfo();
        let t = await EventGetRoleId({ idevent: event.idevent, iduser: Number(user.attributes["custom:iduser"]) })
        history.push("/before/" + String(event.idevent), { event: event, role: t.body.idrole });
    }

    const handleClickDuring = async (event) => {
        let user = await Auth.currentUserInfo();
        let t = await EventGetRoleId({ idevent: event.idevent, iduser: Number(user.attributes["custom:iduser"]) })
        let r = null
        if (t.body.idrole === 4) r = await EventGetTeamMentoring({ idevent: event.idevent, iduser: Number(user.attributes["custom:iduser"]) })
        history.push("/during/" + String(event.idevent), { event: event, role: t.body.idrole, idgroup: r === null ? null : r.body.idgroup });
    }

    const handleClickAfter = async (event) => {
        let user = await Auth.currentUserInfo();
        let t = await EventGetRoleId({ idevent: event.idevent, iduser: Number(user.attributes["custom:iduser"]) })
        history.push("/after/" + String(event.idevent), { event: event, role: t.body.idrole });
    }

    const handleClickJoin = async (event) => {
        let user = await Auth.currentUserInfo();
        let t = await EventGetTeamId({ idevent: event.idevent, iduser: Number(user.attributes["custom:iduser"]) })
        history.push("/hack/" + String(event.idevent), { idevent: event.idevent, eventName: event.name, idgroup: t.body.idgroup });
    }

    return (
        <Grid container spacing={2} style={{ margin: '1rem', maxWidth: '100vw' }}>
            <Grid item sm={12}>
                <Typography variant="h4" gutterBottom>
                    {title}
                </Typography>
                {(!Array.isArray(events) || !events.length) ? <Typography variant="body1">Ningún evento disponible en esta categoría.</Typography> : <></>}
            </Grid>

            {(events.map((event) => {
                return (
                    <Grid key={event.idevent} item sm={3}>
                        <Card className={classes.root}>
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="h2">
                                    {event.name}
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p">
                                    {event.short_desc}
                                </Typography>
                            </CardContent>
                            <CardActions>
                                <Box display={(mode === 'c') ? 'block' : 'none'}>
                                    <Button size="small" onClick={() => handleClickBefore(event)}>Antes</Button>
                                    <Button size="small" onClick={() => handleClickDuring(event)}>Durante</Button>
                                    <Button size="small" onClick={() => handleClickAfter(event)}>Después</Button>
                                </Box>
                                <Box display={(mode === 'p') ? 'block' : 'none'}>
                                    <Button size="small" onClick={() => handleClickJoin(event)}>Ingresar</Button>
                                </Box>
                            </CardActions>
                        </Card>
                    </Grid>)
            }))}
        </Grid>
    );
}

export default EventSimpleList;