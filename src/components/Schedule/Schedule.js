import React, { useEffect, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import Timeline from '@material-ui/lab/Timeline';
import TimelineItem from '@material-ui/lab/TimelineItem';
import TimelineSeparator from '@material-ui/lab/TimelineSeparator';
import TimelineConnector from '@material-ui/lab/TimelineConnector';
import TimelineContent from '@material-ui/lab/TimelineContent';
import TimelineDot from '@material-ui/lab/TimelineDot';
import TimelineOppositeContent from '@material-ui/lab/TimelineOppositeContent';
import { Auth } from 'aws-amplify';
import { ActivityList, ActivityCreate, ActivityUpdateList, ActivityDelete } from '../../api/services';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

let times = [
    { value: '12:00 AM' },
    { value: '12:30 AM' },
    { value: '01:00 AM' },
    { value: '01:30 AM' },
    { value: '02:00 AM' },
    { value: '02:30 AM' },
    { value: '03:00 AM' },
    { value: '03:30 AM' },
    { value: '04:00 AM' },
    { value: '04:30 AM' },
    { value: '05:00 AM' },
    { value: '05:30 AM' },
    { value: '06:00 AM' },
    { value: '06:30 AM' },
    { value: '07:00 AM' },
    { value: '07:30 AM' },
    { value: '08:00 AM' },
    { value: '08:30 AM' },
    { value: '09:00 AM' },
    { value: '09:30 AM' },
    { value: '10:00 AM' },
    { value: '10:30 AM' },
    { value: '11:00 AM' },
    { value: '11:30 AM' },
    { value: '12:00 PM' },
    { value: '12:30 PM' },
    { value: '01:00 PM' },
    { value: '01:30 PM' },
    { value: '02:00 PM' },
    { value: '02:30 PM' },
    { value: '03:00 PM' },
    { value: '03:30 PM' },
    { value: '04:00 PM' },
    { value: '04:30 PM' },
    { value: '05:00 PM' },
    { value: '05:30 PM' },
    { value: '06:00 PM' },
    { value: '06:30 PM' },
    { value: '07:00 PM' },
    { value: '07:30 PM' },
    { value: '08:00 PM' },
    { value: '08:30 PM' },
    { value: '09:00 PM' },
    { value: '09:30 PM' },
    { value: '10:00 PM' },
    { value: '10:30 PM' },
    { value: '11:00 PM' },
    { value: '11:30 PM' },
]

const Schedule = (props) => {
    const event = props.history.location.state.event;
    const [acts, setActs] = useState([]);
    const [openCreate, setOpenCreate] = useState(false);
    const [openSchedule, setOpenSchedule] = useState(false);
    const [name, setName] = useState('');
    const [time, setTime] = useState('');
    const [disabledSave, setDisabledSave] = useState(true);

    const getActs = async () => {
        let t = await ActivityList({ idevent: event.idevent });
        setActs(t.body)
    }

    const handleCreate = async () => {
        let user = await Auth.currentUserInfo();
        let today = new Date();
        await ActivityCreate({ idevent: event.idevent, name: name, time: time, iduser: Number(user.attributes["custom:iduser"]), date: today });
        setOpenCreate(false);
        setActs([]);
        getActs();
    }

    const move = (array, index, delta) => {
        let newIndex = index + delta;
        if (newIndex < 0 || newIndex === array.length) return;
        let indexes = [index, newIndex].sort((a, b) => a - b);
        array.splice(indexes[0], 2, array[indexes[1]], array[indexes[0]]);
    };

    const handleUp = (g, index) => {
        const clone = JSON.parse(JSON.stringify(acts));
        move(clone, index, -1);
        setActs(clone);
        setDisabledSave(false);
    }

    const handleDown = (g, index) => {
        const clone = JSON.parse(JSON.stringify(acts));
        move(clone, index, 1);
        setActs(clone);
        setDisabledSave(false);
    }

    const handleSaveChanges = async () => {
        await ActivityUpdateList({ idevent: event.idevent, acts: acts });
        setActs([]);
        getActs();
        setDisabledSave(true);
    }

    const handleDelete = async (g, index) => {
        let aux = JSON.parse(JSON.stringify(acts));
        aux.splice(index, 1);
        setActs(aux)
        await ActivityDelete({ idactivity: g.idactivity });
    }

    const handleClickCreate = () => { setOpenCreate(true); setName(''); setTime(''); }
    const handleCloseCreate = () => setOpenCreate(false);
    const handleClickSchedule = () => setOpenSchedule(true)
    const handleCloseSchedule = () => setOpenSchedule(false);
    const changeName = (event) => setName(event.target.value);
    const changeTime = (event) => setTime(event.target.value);

    useEffect(() => {
        getActs();
    }, []);


    return (
        <>
            <Typography variant="h6" gutterBottom>
                Cronograma de actividades
            </Typography>
            <Grid container spacing={2} style={{ marginBottom: '1rem' }}>
                <Grid item>
                    <Button variant="contained" color="primary" onClick={handleClickCreate}>Nueva actividad</Button>
                </Grid>
                <Grid item>
                    <Button variant="contained" onClick={handleClickSchedule}>Ver cronograma</Button>
                </Grid>
                <Grid item>
                    <Button variant="contained" color="primary" onClick={handleSaveChanges} disabled={disabledSave}>Guardar cambios</Button>
                </Grid>
            </Grid>
            {(!Array.isArray(acts) || !acts.length) ?
                <Typography>No hay actividades registradas</Typography> :
                <>
                    <TableContainer component={Paper} >
                        <Table aria-label="collapsible table" size={'small'}>
                            <TableHead>
                                <TableRow>
                                    <TableCell align="left">Nombre</TableCell>
                                    <TableCell align="right">Hora</TableCell>
                                    <TableCell align="right">Acciones</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {acts.map((g, index) => (
                                    <TableRow key={g.iduser}>
                                        <TableCell component="th" scope="row">
                                            {g.name}
                                        </TableCell>
                                        <TableCell align="right">{g.time}</TableCell>
                                        <TableCell align="right">
                                            <IconButton aria-label="expand row" size="small" onClick={() => handleUp(g, index)}>
                                                <ArrowUpwardIcon />
                                            </IconButton>
                                            <IconButton aria-label="expand row" size="small" onClick={() => handleDown(g, index)}>
                                                <ArrowDownwardIcon />
                                            </IconButton>
                                            <IconButton aria-label="expand row" size="small" onClick={() => handleDelete(g, index)}>
                                                <DeleteOutlineIcon />
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </>
            }
            <Dialog open={openCreate} onClose={handleCloseCreate} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Agregar actividad</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Ingrese el nombre de la actividad. Opcionalmente puede ingresar la hora de la actividad.
                    </DialogContentText>
                    <TextField
                        autoFocus
                        id="name"
                        variant="outlined"
                        onChange={changeName}
                        value={name}
                        label="Nombre"
                        fullWidth
                    />
                    <br></br>
                    <FormControl variant="outlined" style={{ width: '200px' }}>
                        <InputLabel id="equipo-select">Hora</InputLabel>
                        <Select
                            value={time}
                            onChange={changeTime}
                            label="Hora"
                        >
                            <MenuItem value={''}>
                                <em>Seleccionar</em>
                            </MenuItem>
                            {(times.map((t, index) => {
                                return (
                                    <MenuItem key={index} value={t.value}>{t.value}</MenuItem>
                                )
                            }))}
                        </Select>
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCreate} color="primary">
                        Aceptar
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog open={openSchedule} onClose={handleCloseSchedule} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Cronograma</DialogTitle>
                <DialogContent>
                    <Timeline align="alternate">
                        {acts.map((g) => (
                            <TimelineItem>
                                <TimelineOppositeContent>
                                    <Typography color="textSecondary">{g.time}</Typography>
                                </TimelineOppositeContent>
                                <TimelineSeparator>
                                    <TimelineDot />
                                    <TimelineConnector />
                                </TimelineSeparator>
                                <TimelineContent>
                                    <Typography>{g.name}</Typography>
                                </TimelineContent>
                            </TimelineItem>
                        ))}
                    </Timeline>
                </DialogContent>
            </Dialog>
        </>
    );
}

export default Schedule;