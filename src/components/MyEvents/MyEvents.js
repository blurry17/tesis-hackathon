import React, { useState, useEffect } from 'react';
import EventSimpleList from '../EventSimpleList/EventSimpleList'
import { EventGetByUser, EventGetParticipating } from '../../api/services'
import { Auth } from 'aws-amplify';

const MyEvents = () => {
    const [createdEvents, setCreatedEvents] = useState([]);
    const [participatingEvents, setParticipatingEvents] = useState([]);

    const getCreatedEvents = async () => {
        let user = await Auth.currentUserInfo();
        const data = await EventGetByUser({ idUser: Number(user.attributes["custom:iduser"]) });
        setCreatedEvents(data.body);
    }

    const getParticipatingEvents = async () => {
        let user = await Auth.currentUserInfo();
        const data = await EventGetParticipating({ iduser: Number(user.attributes["custom:iduser"]) });
        setParticipatingEvents(data.body);
    }

    useEffect(() => {
        function exec() {
            getCreatedEvents();
            getParticipatingEvents();
        }
        exec();
    }, []);

    return (
        <>
            <EventSimpleList mode={'c'} title={'Eventos como organizador'} events={createdEvents}></EventSimpleList>
            <EventSimpleList mode={'p'} title={'Eventos como participante'} events={participatingEvents}></EventSimpleList>
        </>
    );
}

export default MyEvents;