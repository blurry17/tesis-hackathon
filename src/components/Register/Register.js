import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { useStyles } from './Register.module'
import { Link } from "react-router-dom";
import { Auth } from 'aws-amplify';
import { useHistory } from "react-router-dom";
import { UserCreate } from '../../api/services';
import { Snackbar } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';

export default function Register() {
    const classes = useStyles();
    const history = useHistory();
    const [openSnack, setOpenSnack] = useState(false);
    const [severitySnack, setSeveritySnack] = useState('success');
    const [messageSnack, setMessageSnack] = useState('');
    const [errors, setErrors] = useState({
        cognito: null,
        blankfield: false,
        passwordmatch: false
    });

    const popSnack = (msg, sev) => {
        setMessageSnack(msg)
        setSeveritySnack(sev)
        setOpenSnack(true)
    }

    const [input, setInput] = useState({
        email: '',
        password: '',
        confirm: '',
        name: '',
        lastName: ''
    })

    const clearErrorState = () => {
        setErrors({
            cognito: null,
            blankfield: false,
            passwordmatch: false
        })
    }

    const handleInputChange = (event) => {
        setInput({ ...input, [event.target.id]: event.target.value })
    }

    const validateInputs = () => {
        if (input.password !== input.confirm) return { passwordmatch: true }
        if (!input.email.trim()) return { blankfield: true };
        if (!input.password.trim()) return { blankfield: true };
        if (!input.confirm.trim()) return { blankfield: true };
        if (!input.name.trim()) return { blankfield: true };
        if (!input.lastName.trim()) return { blankfield: true };
        return null;
    }

    const handleClickRegister = async event => {
        event.preventDefault();

        clearErrorState();
        const error = validateInputs()
        if (error) {
            setErrors({ ...errors, ...error });
            popSnack('Todos los campos deben estar completos y las contraseñas deben coincidir', 'error')
            return;
        }

        if (input.password.length < 8) {
            popSnack('La contraseña debe tener 8 caracteres como mínimo', 'error')
            return;
        }

        if (!(/[a-z]/.test(input.password)) || !(/[0-9]/.test(input.password))) {
            popSnack('La contraseña debe contener al menos una letra y un número', 'error')
            return;
        }

        try {
            let data = await UserCreate({
                name: input.name,
                lastName: input.lastName,
                email: input.email
            });
            await Auth.signUp({
                username: input.email,
                password: input.password,
                attributes: {
                    'custom:name': input.name,
                    'custom:lastName': input.lastName,
                    'custom:iduser': String(data.body.id),
                    email: input.email,
                }
            })
            history.push('/afterRegister')
        } catch (e) {
            console.dir(e)
        }
    }

    return (
        <>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div className={classes.paper}>
                    <Typography component="h1" variant="h5">
                        Crear cuenta
                </Typography>
                    <form className={classes.form} noValidate>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Correo electrónico"
                            name="email"
                            autoComplete="email"
                            onChange={handleInputChange}
                            autoFocus
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="name"
                            label="Nombre"
                            name="name"
                            autoComplete="name"
                            onChange={handleInputChange}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="lastName"
                            label="Apellidos"
                            name="lastName"
                            autoComplete="email"
                            onChange={handleInputChange}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Contraseña"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            onChange={handleInputChange}
                            helperText='Debe contener al menos 8 caracteres, una letra y un número'
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="confirm_password"
                            label="Confirmar contraseña"
                            type="password"
                            id="confirm"
                            autoComplete="current-password"
                            onChange={handleInputChange}
                        />
                        <Button
                            fullWidth
                            variant="contained"
                            color="primary"
                            onClick={handleClickRegister}
                            className={classes.submit}
                            submit
                        >
                            Registrarse
                        </Button>
                        <Grid container>
                            <Grid item xs style={{ 'margin-bottom': '1rem', 'text-align': 'center' }}>
                                <Link to='/login' variant="body1">
                                    ¿Ya tienes cuenta? Inicia sesión
                            </Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Container>
            <Snackbar open={openSnack} autoHideDuration={3000} onClose={() => setOpenSnack(false)}>
                <Alert onClose={() => setOpenSnack(false)} severity={severitySnack}>
                    {messageSnack}
                </Alert>
            </Snackbar>
        </>
    );
}
