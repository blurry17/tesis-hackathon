import React, { useState, useEffect } from 'react';
import { Auth } from 'aws-amplify';
import { NotificationGet, NotificationAcceptTeam, NotificationAcceptEvent, NotificationDelete } from '../../api/services';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { useStyles } from './Alerts.module';
import CheckIcon from '@material-ui/icons/Check';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import Card from '@material-ui/core/Card';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';

const Alerts = () => {
    const [alerts, setAlerts] = useState([])
    const classes = useStyles();

    const handleClickAccept = async (a) => {
        let user = await Auth.currentUserInfo();
        let d = {
            iduser: Number(user.attributes["custom:iduser"]),
            idgroup: a.group_idgroup,
            idnotification: a.idnotification,
            type: a.type
        }
        await NotificationAcceptTeam(d)
        let res = await NotificationGet({ iduser: Number(user.attributes["custom:iduser"]) })
        setAlerts(res.body)
    }

    const handleClickDelete = async (a, index) => {
        let aux = JSON.parse(JSON.stringify(alerts));
        aux.splice(index, 1);
        setAlerts(aux)
        await NotificationDelete({ idnotification: a.idnotification })
    }

    const handleClickAcceptEvent = async (a) => {
        let user = await Auth.currentUserInfo();
        let d = {
            iduser: Number(user.attributes["custom:iduser"]),
            idevent: a.event_idevent,
            idnotification: a.idnotification,
        }
        await NotificationAcceptEvent(d)
        let res = await NotificationGet({ iduser: Number(user.attributes["custom:iduser"]) })
        setAlerts(res.body)
    }

    const getAlerts = async () => {
        let user = await Auth.currentUserInfo();
        let res = await NotificationGet({ iduser: Number(user.attributes["custom:iduser"]) })
        setAlerts(res.body)
    }

    useEffect(() => {
        getAlerts();
    }, []);

    return (
        <>
            <Container component='main' maxWidth='md'>
                <div className={classes.paper}>
                    <Typography variant='h3'>
                        Notificaciones
                    </Typography>
                    <Grid maxWidth>
                        {(alerts.map((a, index) => {
                            if (a.group_idgroup != null)
                                return (
                                    <Card>
                                        <CardHeader
                                            action={
                                                <CardActions>
                                                    <Tooltip title="Aceptar">
                                                        <IconButton onClick={() => handleClickAccept(a)}>
                                                            <CheckIcon />
                                                        </IconButton>
                                                    </Tooltip>
                                                    <Tooltip title="Eliminar">
                                                        <IconButton onClick={() => handleClickDelete(a, index)}>
                                                            <DeleteOutlineIcon />
                                                        </IconButton>
                                                    </Tooltip>
                                                </CardActions>
                                            }
                                            title={'Te invitaron a unirte al equipo: ' + a.group}
                                        />
                                    </Card>
                                )
                            else if (a.event_idevent != null)
                                return (
                                    <Card>
                                        <CardHeader
                                            action={
                                                <CardActions>
                                                    <Tooltip title="Aceptar">
                                                        <IconButton onClick={() => handleClickAcceptEvent(a)}>
                                                            <CheckIcon />
                                                        </IconButton>
                                                    </Tooltip>
                                                    <Tooltip title="Eliminar">
                                                        <IconButton onClick={() => handleClickDelete(a)}>
                                                            <DeleteOutlineIcon />
                                                        </IconButton>
                                                    </Tooltip>
                                                </CardActions>
                                            }
                                            title={'Te invitaron a formar parte del personal de: ' + a.event}
                                        />
                                    </Card>
                                )
                        }))}
                    </Grid>
                </div>
            </Container>
        </>
    );
}

export default Alerts;