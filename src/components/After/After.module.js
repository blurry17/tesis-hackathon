import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    menu: {
        margin: theme.spacing(2)
    },
}));