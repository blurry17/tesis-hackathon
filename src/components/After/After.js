import React, { useState, useEffect } from 'react';
import { Typography, Paper, Grid, Button, List, ListItem, ListItemText, TextField, Divider } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { ReportEnrolledAccepted, ReportCollegeDiversity, ReportSexDiversity, ReportMajorDiversity, ReportStudyLevelDiversity, ReportRatings, ReportComments, EventUploadEvidence, EventGetEvidence, EventUpdateRepository, EventGetRepository } from '../../api/services';
import MuiAlert from '@material-ui/lab/Alert';
import { Bar, Pie } from 'react-chartjs-2';
import Dropzone from '../../utils/UploadFiles/Dropzone';
import { Auth } from 'aws-amplify';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import GetAppIcon from '@material-ui/icons/GetApp';

function Alert(props) {
    return <MuiAlert variant="outlined" {...props} />;
}

const After = (props) => {
    const event = props.history.location.state.event;
    const [openDocs, setOpenDocs] = useState(false);
    const [docs, setDocs] = useState([]);
    const [repo, setRepo] = useState("");
    const [data1, setData1] = useState([]);
    const [labels1, setLabels1] = useState([]);
    const [data2, setData2] = useState([]);
    const [labels2, setLabels2] = useState([]);
    const [data3, setData3] = useState([]);
    const [labels3, setLabels3] = useState([]);
    const [data4, setData4] = useState([]);
    const [labels4, setLabels4] = useState([]);
    const [data5, setData5] = useState([]);
    const [labels5, setLabels5] = useState([]);
    const [data6, setData6] = useState([]);
    const [labels6, setLabels6] = useState([]);
    const [feedback, setFeedback] = useState([]);
    const [files, setFiles] = useState([]);

    const getEnrolledAccepted = async () => {
        let t = await ReportEnrolledAccepted({ idevent: event.idevent });
        setData1([t.body.accepted + t.body.denied, t.body.accepted]);
        setLabels1(['Inscritos', 'Aceptados'])
    }

    const getUniversityDiversity = async () => {
        let t = await ReportCollegeDiversity({ idevent: event.idevent });
        let aux1 = []
        let aux2 = []
        for (let i = 0; i < t.body.length; i++) {
            aux1[i] = t.body[i].count
            aux2[i] = t.body[i].name[0]
        }
        setData2(aux1);
        setLabels2(aux2)
    }

    const getSexDiversity = async () => {
        let t = await ReportSexDiversity({ idevent: event.idevent });
        let aux1 = []
        let aux2 = []
        for (let i = 0; i < t.body.length; i++) {
            aux1[i] = t.body[i].count
            aux2[i] = t.body[i].name[0]
        }
        setData3(aux1);
        setLabels3(aux2)
    }

    const getMajorDiversity = async () => {
        let t = await ReportMajorDiversity({ idevent: event.idevent });
        let aux1 = []
        let aux2 = []
        for (let i = 0; i < t.body.length; i++) {
            aux1[i] = t.body[i].count
            aux2[i] = t.body[i].name[0]
        }
        setData4(aux1);
        setLabels4(aux2)
    }

    const getStudyLevelDiversity = async () => {
        let t = await ReportStudyLevelDiversity({ idevent: event.idevent });
        let aux1 = []
        let aux2 = []
        for (let i = 0; i < t.body.length; i++) {
            aux1[i] = t.body[i].count
            aux2[i] = t.body[i].name[0]
        }
        setData5(aux1);
        setLabels5(aux2)
    }

    const getRatings = async () => {
        let t = await ReportRatings({ idevent: event.idevent });
        let aux1 = []
        let aux2 = []
        for (let i = 0; i < t.body.length; i++) {
            aux1[i] = t.body[i].count
            aux2[i] = Number(t.body[i].name[0])
        }
        setData6(aux1);
        setLabels6(aux2)
    }

    const getFeedback = async () => {
        let t = await ReportComments({ idevent: event.idevent });
        let aux1 = []
        for (let i = 0; i < t.body.length; i++) {
            aux1[i] = t.body[i].comment
        }
        setFeedback(aux1);
    }

    const getDocs = async () => {
        let t = await EventGetEvidence({ idevent: event.idevent })
        setDocs(t.body)
    }

    const getRepository = async () => {
        let t = await EventGetRepository({ idevent: event.idevent })
        setRepo(t.body)
    }

    const handleClickDocs = async () => {
        setOpenDocs(true);
        getRepository();
        getDocs();
    }

    const handleUploadFiles = async () => {
        if (files.length !== 0) {
            let user = await Auth.currentUserInfo();
            let data = { idevent: event.idevent, iduser: Number(user.attributes["custom:iduser"]) }
            await EventUploadEvidence(data, files);
            setFiles([]);
            setDocs([]);
            getDocs();
        }
    }

    const deleteFile = (data) => {
        let aux = [...files]
        aux.map((x, index) => {
            if (x.name === data) aux.splice(index, 1);
            return x;
        })
        setFiles(aux)
    }

    const formatBytes = (bytes, decimals = 2) => {
        if (bytes === 0) return '0 Bytes';
        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        const i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    const onFilesAdded = (fileArray) => {
        let found = false;
        if (files === []) setFiles(prevFiles => files.concat(fileArray));
        else {
            files.map((x) => {
                if (x.name === fileArray[0].name) found = true;
                return x;
            })
            if (!found) setFiles(prevFiles => files.concat(fileArray));
        }
    }

    const handleUpdateRepo = async () => {
        await EventUpdateRepository({ idevent: event.idevent, repository: repo })
    }

    const changeRepo = (event) => setRepo(event.target.value);

    useEffect(() => {
        getEnrolledAccepted()
        getUniversityDiversity()
        getSexDiversity()
        getMajorDiversity()
        getStudyLevelDiversity()
        getRatings()
        getFeedback()
    }, []);

    return (
        <>
            <Grid container style={{ paddingBottom: '5rem' }}>
                <Grid item sm={12}>
                    <Typography variant="h4" style={{ margin: '1rem' }}>
                        {'Estadísticas e información de: ' + event.name}
                    </Typography>
                </Grid>
                <Grid sm={12} container item spacing={1} justify='space-evenly'>
                    <Grid item sm={6}>
                        <Bar
                            data={{
                                datasets: [{ data: data1 }, { backgroundColor: ["#FF9CEE", "#6EB5FF"] }],
                                labels: labels1
                            }}
                            width={400}
                            height={200}
                            options={{
                                title: { display: true, text: 'Equipos inscritos vs. aceptados', fontSize: 24 },
                                legend: { display: false },
                                maintainAspectRatio: false,
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            min: 0,
                                            stepSize: 1
                                        }
                                    }]
                                }
                            }}
                        />
                    </Grid>
                    <Grid item sm={6}>
                        <Pie
                            data={{
                                datasets: [{ data: data2 }, { backgroundColor: ["#FF9CEE", "#6EB5FF", '#AFF8DB', '#FFF5BA', '#DCD3FF', '#FFC9DE'] }],
                                labels: labels2
                            }}
                            width={400}
                            height={400}
                            options={{
                                title: { display: true, text: 'Diversidad de universidades', fontSize: 24 },
                                legend: { display: false },
                                maintainAspectRatio: false
                            }}
                        />
                    </Grid>
                    <Grid item sm={6}>
                        <Pie
                            data={{
                                datasets: [{ data: data3 }, { backgroundColor: ["#6EB5FF", "#FF9CEE", '#AFF8DB', '#FFF5BA', '#DCD3FF', '#FFC9DE'] }],
                                labels: labels3
                            }}
                            width={400}
                            height={400}
                            options={{
                                title: { display: true, text: 'Diversidad de géneros', fontSize: 24 },
                                legend: { display: false },
                                maintainAspectRatio: false
                            }}
                        />
                    </Grid>
                    <Grid item sm={6}>
                        <Pie
                            data={{
                                datasets: [{ data: data4 }, { backgroundColor: ["#FF9CEE", "#6EB5FF", '#AFF8DB', '#FFF5BA', '#DCD3FF', '#FFC9DE'] }],
                                labels: labels4
                            }}
                            width={400}
                            height={400}
                            options={{
                                title: { display: true, text: 'Diversidad de carreras', fontSize: 24 },
                                legend: { display: false },
                                maintainAspectRatio: false
                            }}
                        />
                    </Grid>
                    <Grid item sm={6}>
                        <Pie
                            data={{
                                datasets: [{ data: data5 }, { backgroundColor: ["#FF9CEE", "#6EB5FF", '#AFF8DB', '#FFF5BA', '#DCD3FF', '#FFC9DE'] }],
                                labels: labels5
                            }}
                            width={400}
                            height={400}
                            options={{
                                title: { display: true, text: 'Diversidad de nivel de estudios', fontSize: 24 },
                                legend: { display: false },
                                maintainAspectRatio: false
                            }}
                        />
                    </Grid>
                    <Grid item sm={6}>
                        <Bar
                            data={{
                                datasets: [{ data: data6 }, { backgroundColor: ["#FF9CEE", "#6EB5FF", '#AFF8DB', '#FFF5BA', '#DCD3FF', '#FFC9DE'] }],
                                labels: labels6
                            }}
                            width={100}
                            height={400}
                            options={{
                                title: { display: true, text: 'Puntuación asignada por los participantes', fontSize: 24 },
                                legend: { display: false },
                                maintainAspectRatio: false,
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            min: 0,
                                            stepSize: 1
                                        }
                                    }]
                                }
                            }}
                        />
                    </Grid>
                    <Grid item sm={12}>
                        <Typography variant="h4" style={{ marginLeft: '1rem', marginTop: '1rem' }}>
                            Comentarios de los participantes
                    </Typography>
                    </Grid>
                    <Grid item sm={12}>
                        <List dense={false}>
                            {feedback.map((e) => { return (<ListItem><ListItemText primary={e} /></ListItem>) })}
                        </List>
                    </Grid>
                    <Grid item sm={12}>
                        <Typography variant="h4" style={{ marginLeft: '1rem', marginTop: '1rem' }}>
                            Evidencias del evento
                    </Typography>
                    </Grid>
                    <Grid item sm={12}>
                        <Button variant='contained' onClick={handleClickDocs}>Ver evidencias del evento</Button>
                    </Grid>
                </Grid>
            </Grid>

            <Dialog open={openDocs} onClose={() => setOpenDocs(false)} aria-labelledby="form-dialog-title" fullWidth maxWidth='sm'>
                <DialogTitle id="form-dialog-title">Evidencias del evento</DialogTitle>
                <DialogContent>
                    <TextField
                        margin="dense"
                        onChange={changeRepo}
                        value={repo}
                        label="Enlace al repositorio"
                        InputLabelProps={{ shrink: true }}
                        variant="outlined"
                        fullWidth
                    />
                    <Button variant='contained' onClick={handleUpdateRepo}>Guardar</Button>
                    <Divider style={{ marginTop: '20px', marginBottom: '20px' }}></Divider>
                    <Typography>Opcionalmente, puede guardar archivos directamente aquí:</Typography>
                    <TableContainer component={Paper}>
                        <Table size="small">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Documento</TableCell>
                                    <TableCell align="right">Descargar</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {docs.map((d) => (
                                    <TableRow key={d.iddocument}>
                                        <TableCell component="th" scope="row">
                                            {d.name}
                                        </TableCell>
                                        <TableCell align="right">
                                            <IconButton aria-label="expand row" size="small" onClick={() => window.open(d.file_url)}>
                                                <GetAppIcon />
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <Typography style={{ marginTop: '1rem' }}>Subir archivos aquí:</Typography>
                    <Dropzone
                        onFilesAdded={onFilesAdded}
                        disabled={false}
                        multiple={true}
                        style={{ heigth: '30px' }}
                    />
                    <div>
                        {files.map(file => {
                            return (
                                <Alert key={file.name}
                                    onClose={() => deleteFile(file.name)}>{file.name} - {formatBytes(file.size)}</Alert>
                            )
                        })}
                    </div>
                </DialogContent>
                <DialogActions>
                    <Button color="primary" onClick={handleUploadFiles}>
                        Subir
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    )
};

export default After;