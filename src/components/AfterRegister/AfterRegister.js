import React from 'react';
import { Typography } from '@material-ui/core';


const AfterRegister = () => {

    return (
        <div style={{ 'text-align': 'center' }}>
            <Typography variant='h2'>
                ¡Gracias por registrarte!
            </Typography>
            <Typography variant='h3'>
                Te acabamos de enviar un correo con un enlace para que puedas validar tu cuenta.
            </Typography>
        </div>
    );
}

export default AfterRegister;