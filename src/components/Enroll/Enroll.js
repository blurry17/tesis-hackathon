import React, { useEffect, useState } from 'react';
import { EventGetEnrolledG, EventGetEnrolledI, EventGetAccepted, EventDenyTeam, EventAcceptTeam, TeamCreateTemp, UserGetInfo, TeamGetMembers } from '../../api/services';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import Tooltip from '@material-ui/core/Tooltip';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import CheckIcon from '@material-ui/icons/Check';
import ClearIcon from '@material-ui/icons/Clear';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import InfoIcon from '@material-ui/icons/Info';
import Checkbox from '@material-ui/core/Checkbox';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from '@material-ui/core/Grid';
import Snackbar from '@material-ui/core/Snackbar';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import { useStyles } from './Enroll.module';
import Alert from '@material-ui/lab/Alert';
import clsx from 'clsx';

const Enroll = (props) => {
    const event = props.history.location.state.event;
    const classes = useStyles()
    const [enrolledG, setEnrolledG] = useState([]);
    const [enrolledI, setEnrolledI] = useState([]);
    const [accepted, setAccepted] = useState([]);
    const [members, setMembers] = useState([]);
    const [openGroup, setOpenGroup] = useState(false);
    const [openInfoG, setOpenInfoG] = useState(false);
    const [openInfoI, setOpenInfoI] = useState(false);
    const [currentUser, setCurrentUser] = useState(null);
    const [nameNewGroup, setNameNewGroup] = useState('');
    const [openSnack, setOpenSnack] = useState(false);
    const [severitySnack, setSeveritySnack] = useState('success');
    const [messageSnack, setMessageSnack] = useState('');

    const popSnack = (msg, sev) => {
        setMessageSnack(msg)
        setSeveritySnack(sev)
        setOpenSnack(true)
    }

    const getEnrolledG = async () => {
        if (event.registration_type === 2 || event.registration_type === 3) {
            let t = await EventGetEnrolledG({ idevent: event.idevent });
            setEnrolledG(t.body)
        }
    }

    const getEnrolledI = async () => {
        if (event.registration_type === 1 || event.registration_type === 3) {
            let t = await EventGetEnrolledI({ idevent: event.idevent });
            for (let i = 0; i < t.body.length; i++) t.body[i].checked = false;
            setEnrolledI(t.body)
        }
    }

    const getAccepted = async () => {
        let t = await EventGetAccepted({ idevent: event.idevent });
        setAccepted(t.body)
    }

    const handleAcceptTeam = async (g) => {
        await EventAcceptTeam({ idevent: event.idevent, idgroup: g.idgroup });
        setEnrolledG([]);
        getEnrolledG();
        setAccepted([]);
        getAccepted();
    }

    const handleDenyTeam = async (g) => {
        await EventDenyTeam({ idevent: event.idevent, idgroup: g.idgroup });
        getEnrolledG();
    }

    const handleSeeInfoG = async (g) => {
        let t = await TeamGetMembers({ idgroup: g.idgroup });
        for (let i = 0; i < t.body.length; i++) t.body[i].expanded = false;
        setMembers(t.body)
        setOpenInfoG(true);
    }

    const handleSeeInfoI = async (g) => {
        let t = await UserGetInfo({ iduser: g.id });
        setCurrentUser(t.body)
        setOpenInfoI(true);
    }

    const changeCheckRow = async (g, index) => {
        let clone = Array.from(enrolledI)
        clone[index].checked = !(clone[index].checked);
        setEnrolledI(clone)
    }

    const handleClickGroup = () => {
        let count = 0
        for (let i = 0; i < enrolledI.length; i++) if (enrolledI[i].checked) count = count + 1;
        if (count < 2) {
            popSnack('Debe seleccionar como mínimo a 2 postulantes', 'error')
            return;
        }
        setNameNewGroup('');
        setOpenGroup(true);
    }

    const handleCreateNewGroup = async () => {
        let arr = []
        for (let i = 0; i < enrolledI.length; i++) if (enrolledI[i].checked) arr.push(enrolledI[i].id)
        let data = {
            idevent: event.idevent,
            name: nameNewGroup,
            users: arr
        }
        await TeamCreateTemp(data);
        setEnrolledI([]);
        getEnrolledI();
        getAccepted();
        setOpenGroup(false);
    }

    const handleCloseGroup = () => setOpenGroup(false);
    const handleCloseInfoG = () => setOpenInfoG(false);
    const handleCloseInfoI = () => setOpenInfoI(false);
    const changeName = (event) => setNameNewGroup(event.target.value);
    const handleExpandClick = (g, index) => {
        let clone = Array.from(members)
        clone[index].expanded = !(clone[index].expanded);
        setMembers(clone)
    }

    useEffect(() => {
        getEnrolledG();
        getEnrolledI();
        getAccepted()
    }, []);


    return (
        <>
            <Box display={(!Array.isArray(enrolledG) || !enrolledG.length) ? 'none' : 'block'}>
                <Typography variant="h6" gutterBottom>
                    Postulaciones grupales
                </Typography>
                <TableContainer component={Paper}>
                    <Table aria-label="collapsible table" size={'small'}>
                        <TableHead>
                            <TableRow>
                                <TableCell align="left">Grupo</TableCell>
                                <TableCell align="right">Integrantes</TableCell>
                                <TableCell align="right">Acciones</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {enrolledG.map((g) => (
                                <TableRow key={g.idgroup}>
                                    <TableCell component="th" scope="row">
                                        {g.name}
                                    </TableCell>
                                    <TableCell align="right">{g.n_users}</TableCell>
                                    <TableCell align="right">
                                        <Tooltip title="Aceptar inscripción">
                                            <IconButton aria-label="expand row" size="small" onClick={() => handleAcceptTeam(g)}>
                                                <CheckIcon />
                                            </IconButton>
                                        </Tooltip>
                                        <Tooltip title="Rechazar inscripción integrante">
                                            <IconButton aria-label="expand row" size="small" onClick={() => handleDenyTeam(g)}>
                                                <ClearIcon />
                                            </IconButton>
                                        </Tooltip>
                                        <Tooltip title="Ver información">
                                            <IconButton aria-label="expand row" size="small" onClick={() => handleSeeInfoG(g)}>
                                                <InfoIcon />
                                            </IconButton>
                                        </Tooltip>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Box>
            <Box display={(!Array.isArray(enrolledI) || !enrolledI.length) ? 'none' : 'block'} marginTop={2}>
                <Typography variant="h6" gutterBottom>
                    Postulaciones individuales
                </Typography>
                <Button variant="contained" color="primary" onClick={handleClickGroup}>Agrupar</Button>
                <TableContainer component={Paper}>
                    <Table aria-label="collapsible table" size={'small'}>
                        <TableHead>
                            <TableRow>
                                <TableCell align="left" style={{ width: '10px' }}></TableCell>
                                <TableCell align="left">Nombre</TableCell>
                                <TableCell align="right">Acciones</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {enrolledI.map((g, index) => (
                                <TableRow key={g.id}>
                                    <TableCell align="left" style={{ width: '10px' }}>
                                        <Checkbox
                                            checked={g.checked}
                                            color={'primary'}
                                            onClick={() => changeCheckRow(g, index)}
                                            size={'small'}
                                        />
                                    </TableCell>
                                    <TableCell component="th" scope="row">
                                        {g.name + ' ' + g.lastName}
                                    </TableCell>
                                    <TableCell align="right">
                                        <Tooltip title="Ver información">
                                            <IconButton aria-label="expand row" size="small" onClick={() => handleSeeInfoI(g)}>
                                                <InfoIcon />
                                            </IconButton>
                                        </Tooltip>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Box>
            <Box display={(!Array.isArray(accepted) || !accepted.length) ? 'none' : 'block'} marginTop={2}>
                <Typography variant="h6" gutterBottom>
                    Grupos aceptados
                </Typography>
                <TableContainer component={Paper}>
                    <Table aria-label="collapsible table" size={'small'}>
                        <TableHead>
                            <TableRow>
                                <TableCell align="left">Grupo</TableCell>
                                <TableCell align="right">Integrantes</TableCell>
                                <TableCell align="right">Acciones</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {accepted.map((g) => (
                                <TableRow key={g.idgroup}>
                                    <TableCell component="th" scope="row">
                                        {g.name}
                                    </TableCell>
                                    <TableCell align="right">{g.n_users}</TableCell>
                                    <TableCell align="right">
                                        <Tooltip title="Ver información">
                                            <IconButton aria-label="expand row" size="small" onClick={() => handleSeeInfoG(g)}>
                                                <InfoIcon />
                                            </IconButton>
                                        </Tooltip>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Box>
            <Dialog open={openGroup} onClose={handleCloseGroup} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Agrupar postulantes</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Al hacer click en "Agrupar" se creará un grupo con los postulantes seleccionados y automáticamente será inscrito y aceptado en el evento. A continuación ingrese el nombre del nuevo grupo:
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        onChange={changeName}
                        value={nameNewGroup}
                        label="Nombre del equipo"
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button color="primary" onClick={handleCreateNewGroup}>
                        Agrupar
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog open={openInfoG} onClose={handleCloseInfoG} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Información de equipo</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        A continuación se muestra información sobre los integrantes de este equipo.
                    </DialogContentText>
                    {members.map((g, index) => (
                        <Card key={g.iduser} style={{ minWidth: 275 }}>
                            <CardHeader
                                action={
                                    <IconButton
                                        className={clsx(classes.expand, {
                                            [classes.expandOpen]: g.expanded,
                                        })}
                                        onClick={() => handleExpandClick(g, index)}
                                        aria-label="show more"
                                    >
                                        <ExpandMoreIcon />
                                    </IconButton>
                                }
                                title={g.name}
                            />
                            <Collapse in={g.expanded} timeout="auto" unmountOnExit>
                                <CardContent>
                                    <Grid container spacing={2}>
                                        <Grid item xs={12}>
                                            <TextField
                                                label="Nombre"
                                                fullWidth
                                                InputProps={{
                                                    readOnly: true,
                                                }}
                                                InputLabelProps={{ shrink: true }}
                                                variant="outlined"
                                                value={g.name}
                                            />
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField
                                                label="Email"
                                                fullWidth
                                                InputProps={{
                                                    readOnly: true,
                                                }}
                                                InputLabelProps={{ shrink: true }}
                                                variant="outlined"
                                                value={g.email}
                                            />
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField
                                                label="Género"
                                                fullWidth
                                                InputProps={{
                                                    readOnly: true,
                                                }}
                                                InputLabelProps={{ shrink: true }}
                                                variant="outlined"
                                                value={g.gender}
                                            />
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField
                                                label="Carrera"
                                                fullWidth
                                                InputProps={{
                                                    readOnly: true,
                                                }}
                                                InputLabelProps={{ shrink: true }}
                                                variant="outlined"
                                                value={g.major}
                                            />
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField
                                                label="Nivel de estudios"
                                                fullWidth
                                                InputProps={{
                                                    readOnly: true,
                                                }}
                                                InputLabelProps={{ shrink: true }}
                                                variant="outlined"
                                                value={g.studylvl}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField
                                                label="Universidad/Instituto"
                                                fullWidth
                                                InputProps={{
                                                    readOnly: true,
                                                }}
                                                InputLabelProps={{ shrink: true }}
                                                variant="outlined"
                                                value={g.school}
                                            />
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField
                                                label="Teléfono"
                                                fullWidth
                                                InputProps={{
                                                    readOnly: true,
                                                }}
                                                InputLabelProps={{ shrink: true }}
                                                variant="outlined"
                                                value={g.phone}
                                            />
                                        </Grid>
                                        <Grid item xs={6}>
                                            <TextField
                                                label="Fecha de nacimiento"
                                                fullWidth
                                                InputProps={{
                                                    readOnly: true,
                                                }}
                                                InputLabelProps={{ shrink: true }}
                                                variant="outlined"
                                                value={g.bday_day + '/' + g.bday_month + '/' + g.bday_year}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField
                                                label="Descripción"
                                                fullWidth
                                                multiline
                                                InputProps={{
                                                    readOnly: true,
                                                }}
                                                InputLabelProps={{ shrink: true }}
                                                variant="outlined"
                                                value={g.description}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField
                                                label="Preferencia alimenticia"
                                                fullWidth
                                                InputProps={{
                                                    readOnly: true,
                                                }}
                                                InputLabelProps={{ shrink: true }}
                                                variant="outlined"
                                                value={g.diet}
                                            />
                                        </Grid>
                                        <Grid item xs={12}>
                                            <TextField
                                                label="Necesidades especiales"
                                                fullWidth
                                                multiline
                                                InputProps={{
                                                    readOnly: true,
                                                }}
                                                InputLabelProps={{ shrink: true }}
                                                variant="outlined"
                                                value={g.needs}
                                            />
                                        </Grid>
                                    </Grid>
                                </CardContent>
                            </Collapse>
                        </Card>))}
                </DialogContent>
            </Dialog>
            <Dialog open={openInfoI} onClose={handleCloseInfoI} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Información del postulante</DialogTitle>
                <DialogContent>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                label="Nombre"
                                fullWidth
                                InputProps={{
                                    readOnly: true,
                                }}
                                InputLabelProps={{ shrink: true }}
                                variant="outlined"
                                value={currentUser ? currentUser.name : ''}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                label="Email"
                                fullWidth
                                InputProps={{
                                    readOnly: true,
                                }}
                                InputLabelProps={{ shrink: true }}
                                variant="outlined"
                                value={currentUser ? currentUser.email : ''}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                label="Género"
                                fullWidth
                                InputProps={{
                                    readOnly: true,
                                }}
                                InputLabelProps={{ shrink: true }}
                                variant="outlined"
                                value={currentUser ? currentUser.gender : ''}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                label="Carrera"
                                fullWidth
                                InputProps={{
                                    readOnly: true,
                                }}
                                InputLabelProps={{ shrink: true }}
                                variant="outlined"
                                value={currentUser ? currentUser.major : ''}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                label="Nivel de estudios"
                                fullWidth
                                InputProps={{
                                    readOnly: true,
                                }}
                                InputLabelProps={{ shrink: true }}
                                variant="outlined"
                                value={currentUser ? currentUser.studylvl : ''}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                label="Universidad/Instituto"
                                fullWidth
                                InputProps={{
                                    readOnly: true,
                                }}
                                InputLabelProps={{ shrink: true }}
                                variant="outlined"
                                value={currentUser ? currentUser.school : ''}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                label="Teléfono"
                                fullWidth
                                InputProps={{
                                    readOnly: true,
                                }}
                                InputLabelProps={{ shrink: true }}
                                variant="outlined"
                                value={currentUser ? currentUser.phone : ''}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                label="Fecha de nacimiento"
                                fullWidth
                                InputProps={{
                                    readOnly: true,
                                }}
                                InputLabelProps={{ shrink: true }}
                                variant="outlined"
                                value={currentUser ? currentUser.bday_day + '/' + currentUser.bday_month + '/' + currentUser.bday_year : ''}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                label="Descripción"
                                fullWidth
                                multiline
                                InputProps={{
                                    readOnly: true,
                                }}
                                InputLabelProps={{ shrink: true }}
                                variant="outlined"
                                value={currentUser ? currentUser.description : ''}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                label="Preferencia alimenticia"
                                fullWidth
                                InputProps={{
                                    readOnly: true,
                                }}
                                InputLabelProps={{ shrink: true }}
                                variant="outlined"
                                value={currentUser ? currentUser.diet : ''}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                label="Necesidades especiales"
                                fullWidth
                                multiline
                                InputProps={{
                                    readOnly: true,
                                }}
                                InputLabelProps={{ shrink: true }}
                                variant="outlined"
                                value={currentUser ? currentUser.needs : ''}
                            />
                        </Grid>
                    </Grid>
                </DialogContent>
            </Dialog>
            <Snackbar open={openSnack} autoHideDuration={3000} onClose={() => setOpenSnack(false)}>
                <Alert onClose={() => setOpenSnack(false)} severity={severitySnack}>
                    {messageSnack}
                </Alert>
            </Snackbar>
        </>
    );
}

export default Enroll;