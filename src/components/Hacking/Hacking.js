import React, { useState, useEffect } from 'react';
import { Typography, Box, ListItem, Divider, ListItemText, Grid, Button, List, TextField, IconButton, Snackbar, Paper } from '@material-ui/core';
import Rating from '@material-ui/lab/Rating';
import AssignmentIcon from '@material-ui/icons/Assignment';
import ScheduleIcon from '@material-ui/icons/Schedule';
import FeedbackIcon from '@material-ui/icons/Feedback';
import { useStyles } from '../Hacking/Hacking.module';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Auth } from 'aws-amplify';
import { WorkListByGroup, WorkUpload, EventListNews, EventGetMessages, WorkDelete, EventGetQuestions, EventSendMessage, EventSendQuestion, ActivityList, EventSendFeedback, EventDownloadRules, TeamGetScores } from '../../api/services';
import Dropzone from '../../utils/UploadFiles/Dropzone';
import Alert from '@material-ui/lab/Alert';
import SendIcon from '@material-ui/icons/Send';
import Timeline from '@material-ui/lab/Timeline';
import TimelineItem from '@material-ui/lab/TimelineItem';
import TimelineSeparator from '@material-ui/lab/TimelineSeparator';
import TimelineConnector from '@material-ui/lab/TimelineConnector';
import TimelineContent from '@material-ui/lab/TimelineContent';
import TimelineDot from '@material-ui/lab/TimelineDot';
import TimelineOppositeContent from '@material-ui/lab/TimelineOppositeContent';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import GetAppIcon from '@material-ui/icons/GetApp';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import RateReviewIcon from '@material-ui/icons/RateReview';

const Hacking = (props) => {
    const idevent = props.history.location.state.idevent;
    const idgroup = props.history.location.state.idgroup;
    const eventName = props.history.location.state.eventName;
    const classes = useStyles();
    const [ans, setAns] = useState([]);
    const [files, setFiles] = useState([]);
    const [docs, setDocs] = useState([]);
    const [openUpload, setOpenUpload] = useState(false);
    const [msgA, setMsgA] = useState([]);
    const [msgM, setMsgM] = useState([]);
    const [messageA, setMessageA] = useState('');
    const [messageM, setMessageM] = useState('');
    const [acts, setActs] = useState([]);
    const [openSchedule, setOpenSchedule] = useState(false);
    const [openRating, setOpenRating] = useState(false);
    const [openReview, setOpenReview] = useState(false);
    const [reviews, setReviews] = useState([]);
    const [rating, setRating] = useState(5);
    const [ratingMsg, setRatingMsg] = useState('');
    const [openSnack, setOpenSnack] = useState(false);
    const [severitySnack, setSeveritySnack] = useState('success');
    const [messageSnack, setMessageSnack] = useState('');

    const popSnack = (msg, sev) => {
        setMessageSnack(msg)
        setSeveritySnack(sev)
        setOpenSnack(true)
    }

    const getNews = async () => {
        let t = await EventListNews({ idevent: idevent })
        if (t.body !== null) setAns(t.body)
    }

    const getMsgAse = async () => {
        let t = await EventGetMessages({ idevent: idevent })
        if (t.body !== null) setMsgA(t.body)
    }

    const sendMsgA = async () => {
        if (messageA.trim() === "") { setMessageA(''); return; }
        let user = await Auth.currentUserInfo();
        let today = new Date()
        let data = {
            idevent: idevent,
            text: messageA,
            iduser: Number(user.attributes["custom:iduser"]),
            date: today
        }
        await EventSendMessage(data)
        setMessageA('');
        getMsgAse();
    }

    const sendMsgM = async () => {
        if (messageM.trim() === "") { setMessageM(''); return; }
        let user = await Auth.currentUserInfo();
        let today = new Date()
        let data = {
            idevent: idevent,
            idgroup: idgroup,
            text: messageM,
            iduser: Number(user.attributes["custom:iduser"]),
            date: today
        }
        await EventSendQuestion(data)
        setMessageM('');
        getMsgMen();
    }

    const getMsgMen = async () => {
        let t = await EventGetQuestions({ idevent: idevent, idgroup: idgroup })
        if (t.body !== null) setMsgM(t.body)
    }

    const getDocs = async () => {
        let t = await WorkListByGroup({ idevent: idevent, idgroup: idgroup })
        setDocs(t.body)
    }

    const deleteFile = (data) => {
        let aux = [...files]
        aux.map((x, index) => {
            if (x.name === data) aux.splice(index, 1);
            return x;
        })
        setFiles(aux)
    }

    const formatBytes = (bytes, decimals = 2) => {
        if (bytes === 0) return '0 Bytes';
        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        const i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    const onFilesAdded = (fileArray) => {
        let found = false;
        if (files === []) setFiles(prevFiles => files.concat(fileArray));
        else {
            files.map((x) => {
                if (x.name === fileArray[0].name) found = true;
                return x;
            })
            if (!found) setFiles(prevFiles => files.concat(fileArray));
        }
    }

    const handleClickUpload = () => {
        getDocs();
        setOpenUpload(true);
    }

    const handleClickDelete = async (g, index) => {
        let t = await WorkDelete({ idwork: g.idwork, idevent: idevent, idgroup: idgroup, filename: g.name })
        if (t.body.success) getDocs();
    }

    const handleUploadFiles = async () => {
        if (files.length !== 0) {
            let user = await Auth.currentUserInfo();
            let data = { idevent: idevent, idgroup: idgroup, iduser: Number(user.attributes["custom:iduser"]) }
            await WorkUpload(data, files);
            setFiles([]);
            setOpenUpload(false);
            popSnack('Entregable enviado', 'success');
        }
    }

    const handleClickSchedule = async () => {
        setOpenSchedule(true);
        let t = await ActivityList({ idevent: idevent });
        setActs(t.body)
    }

    const handleClickRating = () => {
        setOpenRating(true);
        setRating(5);
        setRatingMsg('');
    }

    const handleSendMessage = async () => {
        let t = await EventSendFeedback({ idevent: idevent, rating: rating, feedback: ratingMsg });
        if (t.body.success) popSnack('Feedback enviado', 'success')
        setOpenRating(false);
    }

    const handleClickDownloadRules = async () => {
        let t = await EventDownloadRules({ idevent: idevent });
        window.open(t.body.file_url);
    }

    const handleClickReview = async () => {
        let t = await TeamGetScores({ idevent: idevent, idgroup: idgroup });
        setReviews(t.body)
        setOpenReview(true);
    }

    const handleCloseUpload = () => setOpenUpload(false);
    const changeMessageA = (event) => setMessageA(event.target.value);
    const changeMessageM = (event) => setMessageM(event.target.value);

    useEffect(() => {
        getMsgAse();
        getMsgMen();
        getNews();
        const interval1 = setInterval(() => getNews(), 30000);
        const interval2 = setInterval(() => getMsgMen(), 10000);
        const interval3 = setInterval(() => getMsgAse(), 10000);
        return () => {
            clearInterval(interval1);
            clearInterval(interval2);
            clearInterval(interval3);
        }
    }, []);

    return (
        <>
            <Box style={{ backgroundColor: '#f0f0f0', textAlign: 'center' }}>
                <Typography variant="h5">
                    {eventName + ' | Participante'}
                </Typography>
            </Box>
            <Divider style={{ border: '1px solid #c0c0c0', padding: '1px', backgroundColor: '#f0f0f0' }}></Divider>
            <Grid container spacing={1} justify='space-evenly' alignItems='flex-start'>
                <Grid item sm={3}>
                    <Typography variant="h4" gutterBottom align='center'>
                        Anuncios
                    </Typography>
                    <List className={classes.listclass}>
                        {ans.map((a, index) => (
                            <div key={a.idnews}>
                                <ListItem alignItems="flex-start">
                                    <ListItemText primary={a.info} />
                                </ListItem>
                                <Divider />
                            </div>
                        ))}
                    </List>
                </Grid>
                <Grid item sm={3}>
                    <Typography variant="h4" gutterBottom align='center'>
                        Mentoría
                    </Typography>
                    <List className={classes.listclass}>
                        {msgM.map((a) => (
                            <div key={a.idquestion}>
                                <ListItem alignItems="flex-start">
                                    <ListItemText primary={a.text} secondary={a.name + ' • ' + a.date} />
                                </ListItem>
                                <Divider />
                            </div>
                        ))}
                    </List>
                    <TextField
                        margin="dense"
                        onChange={changeMessageM}
                        value={messageM}
                        fullWidth
                        variant='outlined'
                        multiline
                        rowsMax={3}
                        InputProps={{ endAdornment: <IconButton onClick={sendMsgM} size='small'><SendIcon /></IconButton> }}
                    />
                </Grid>
                <Grid item sm={3}>
                    <Typography variant="h4" gutterBottom align='center'>
                        Asesoría
                    </Typography>
                    <List className={classes.listclass}>
                        {msgA.map((a) => (
                            <div key={a.idmessage}>
                                <ListItem alignItems="flex-start"><ListItemText primary={a.text} secondary={a.name + ' • ' + a.date} /></ListItem>
                                <Divider />
                            </div>
                        ))}
                    </List>
                    <TextField
                        margin="dense"
                        onChange={changeMessageA}
                        value={messageA}
                        fullWidth
                        variant='outlined'
                        multiline
                        rowsMax={3}
                        InputProps={{ endAdornment: <IconButton onClick={sendMsgA} size='small'><SendIcon /></IconButton> }}
                    />
                </Grid>
                <Grid item sm={3}>
                    <Typography variant="h4" gutterBottom align='center'>
                        Opciones
                    </Typography>
                    <Box style={{ textAlign: 'center' }}>
                        <Button
                            variant="contained"
                            className={classes.button}
                            startIcon={<GetAppIcon />}
                            onClick={handleClickDownloadRules}
                        >
                            Descargar bases
                        </Button>
                        <Button
                            variant="contained"
                            className={classes.button}
                            startIcon={<ScheduleIcon />}
                            onClick={handleClickSchedule}
                        >
                            Ver cronograma
                        </Button>
                        <Button
                            variant="contained"
                            color="primary"
                            className={classes.button}
                            startIcon={<AssignmentIcon />}
                            onClick={handleClickUpload}
                        >
                            Presentar entregables
                        </Button>
                        <Button
                            variant="contained"
                            color="primary"
                            className={classes.button}
                            startIcon={<RateReviewIcon />}
                            onClick={handleClickReview}
                        >
                            Revisar calificación
                        </Button>
                        <Button
                            variant="contained"
                            color="secondary"
                            className={classes.button}
                            startIcon={<FeedbackIcon />}
                            onClick={handleClickRating}
                        >
                            Enviar feedback
                        </Button>
                    </Box>
                </Grid>
            </Grid>
            <Dialog open={openUpload} onClose={handleCloseUpload} aria-labelledby="form-dialog-title" fullWidth maxWidth='sm'>
                <DialogTitle id="form-dialog-title">Entregables</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        En esta ventana debe subir sus documentos para que sean calificados. A continuación puede ver una lista de los archivos subidos por tu equipo hasta el momento:
                    </DialogContentText>
                    <TableContainer component={Paper}>
                        <Table size="small">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Documento</TableCell>
                                    <TableCell align="right">Acciones</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {docs.map((d, index) => (
                                    <TableRow key={d.idwork}>
                                        <TableCell component="th" scope="row">
                                            {d.name}
                                        </TableCell>
                                        <TableCell align="right">
                                            <IconButton aria-label="expand row" size="small" onClick={() => window.open(d.file_url)}>
                                                <GetAppIcon />
                                            </IconButton>
                                            <IconButton aria-label="expand row" size="small" onClick={() => handleClickDelete(d, index)}>
                                                <DeleteOutlineIcon />
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <Typography style={{ marginTop: '1rem' }}>Subir archivos aquí:</Typography>
                    <Dropzone
                        onFilesAdded={onFilesAdded}
                        disabled={false}
                        multiple={true}
                        style={{ heigth: '30px' }}
                    />
                    <div className={classes.files}>
                        {files.map(file => {
                            return (
                                <Alert key={file.name}
                                    onClose={() => deleteFile(file.name)}>{file.name} - {formatBytes(file.size)}</Alert>
                            )
                        })}
                    </div>
                </DialogContent>
                <DialogActions>
                    <Button color="primary" onClick={handleUploadFiles}>
                        Subir
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog open={openSchedule} onClose={() => setOpenSchedule(false)} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Cronograma</DialogTitle>
                <DialogContent>
                    <Timeline align="alternate">
                        {acts.map((g) => (
                            <TimelineItem>
                                <TimelineOppositeContent>
                                    <Typography color="textSecondary">{g.time}</Typography>
                                </TimelineOppositeContent>
                                <TimelineSeparator>
                                    <TimelineDot />
                                    <TimelineConnector />
                                </TimelineSeparator>
                                <TimelineContent>
                                    <Typography>{g.name}</Typography>
                                </TimelineContent>
                            </TimelineItem>
                        ))}
                    </Timeline>
                </DialogContent>
            </Dialog>
            <Dialog open={openRating} onClose={() => setOpenRating(false)} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Retroalimentación</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        En esta sección puedes enviar una retroalimentación totalmente anónima a los organizadores del evento. Por favor, asigne una calificación al desempeño general del evento y, opcionalmente, escriba un mensaje.
                    </DialogContentText>
                    <Typography component='legend'>Calificación:</Typography>
                    <Rating
                        name="simple-controlled"
                        value={rating}
                        onChange={(event, newValue) => {
                            setRating(newValue);
                        }}
                    />
                    <br />
                    <Typography component='legend'>Mensaje</Typography>
                    <TextField
                        margin="dense"
                        onChange={(event) => setRatingMsg(event.target.value)}
                        value={ratingMsg}
                        fullWidth
                        variant='outlined'
                        multiline
                        rowsMax={4}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleSendMessage} color="primary">
                        Enviar
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog open={openReview} onClose={() => setOpenReview(false)} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Calificación</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        {(!Array.isArray(reviews) || !reviews.length) ? "Calificaciones no disponibles en este momento" : "A continuación puede ver la calificación otorgada por el jurado y comentarios de retroalimentación."}
                    </DialogContentText>
                    {reviews.map((r) => {
                        return (
                            <>
                                <Typography variant="h5">{r.name + ': ' + r.score + '/' + r.max}</Typography>
                                {r.comments.map((c) => { return (<Typography variant="body1">{c}</Typography>) })}
                                <br></br>
                            </>
                        )
                    })}
                </DialogContent>
            </Dialog>
            <Snackbar open={openSnack} autoHideDuration={3000} onClose={() => setOpenSnack(false)}>
                <Alert onClose={() => setOpenSnack(false)} severity={severitySnack}>
                    {messageSnack}
                </Alert>
            </Snackbar>
        </>
    )
};

export default Hacking;