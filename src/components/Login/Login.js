import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Snackbar from '@material-ui/core/Snackbar';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { useStyles } from './Login.module'
import { Link } from "react-router-dom";
import { Auth } from 'aws-amplify';
import { useHistory } from "react-router-dom";
import Alert from '@material-ui/lab/Alert';

export default function Login() {
    const classes = useStyles();
    const history = useHistory();
    const [openSnack, setOpenSnack] = useState(false);
    const [severitySnack, setSeveritySnack] = useState('success');
    const [messageSnack, setMessageSnack] = useState('');
    const [input, setInput] = useState({
        email: '',
        password: ''
    })

    const popSnack = (msg, sev) => {
        setMessageSnack(msg)
        setSeveritySnack(sev)
        setOpenSnack(true)
    }

    const handleInputChange = (event) => {
        setInput({ ...input, [event.target.id]: event.target.value })
    }

    const ValidateLogin = async () => {
        try {
            await Auth.signIn(input.email, input.password)
            let u = await Auth.currentSession();
            localStorage.setItem('accessToken', u.accessToken.jwtToken)
            history.push("/");
            window.location.reload();
        } catch (e) {
            popSnack(e.message, 'error')
        }
    }

    return (
        <>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div className={classes.paper}>
                    <Typography component="h1" variant="h5">
                        Login
                </Typography>
                    <form className={classes.form} noValidate>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Correo electrónico"
                            name="email"
                            autoComplete="email"
                            autoFocus
                            onChange={handleInputChange}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Contraseña"
                            type="password"
                            id="password"
                            autoComplete="password"
                            onChange={handleInputChange}
                        />
                        <Button
                            fullWidth
                            variant="contained"
                            color="primary"
                            onClick={ValidateLogin}
                            className={classes.submit}
                        >
                            Ingresar
                        </Button>
                        <Grid container>
                            <Grid item xs>
                                <Link to='forgotPassword' variant="body2">
                                    Olvidé mi contraseña
                            </Link>
                            </Grid>
                            <Grid item>
                                <Link to="/register" variant="body2">
                                    ¿Eres nuevo? ¡Crea una cuenta!
                            </Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Container>
            <Snackbar open={openSnack} autoHideDuration={3000} onClose={() => setOpenSnack(false)}>
                <Alert onClose={() => setOpenSnack(false)} severity={severitySnack}>
                    {messageSnack}
                </Alert>
            </Snackbar>
        </>
    );
}
