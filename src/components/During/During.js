import React, { useState, useEffect } from 'react';
import { Typography, Paper, Divider, Grid, Button, Box, TextField, Snackbar } from '@material-ui/core';
import AssignmentIcon from '@material-ui/icons/Assignment';
import { useStyles } from '../Hacking/Hacking.module';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Auth } from 'aws-amplify';
import { EventGetActive, WorkListByGroup, CriteriaGetJudgeScore, CriteriaSaveScore, EventListNews, EventAddNews, EventGetMessages, EventGetQuestions, EventSendMessage, EventSendQuestion, CriteriaGetAllScores, EventFinishHacking } from '../../api/services';
import MuiAlert from '@material-ui/lab/Alert';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import IconButton from '@material-ui/core/IconButton';
import SendIcon from '@material-ui/icons/Send';
import SpellcheckIcon from '@material-ui/icons/Spellcheck';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import GetAppIcon from '@material-ui/icons/GetApp';
import AddIcon from '@material-ui/icons/Add';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const During = (props) => {
    const event = props.history.location.state.event;
    const role = props.history.location.state.role;
    const idgroup = props.history.location.state.idgroup;
    const classes = useStyles();
    const [ans, setAns] = useState([]);
    const [groups, setGroups] = useState([]);
    const [currentGroup, setCurrentGroup] = useState({ idgroup: null });
    const [docs, setDocs] = useState([]);
    const [openDocs, setOpenDocs] = useState(false);
    const [openFinish, setOpenFinish] = useState(false);
    const [openScores, setOpenScores] = useState(false);
    const [score, setScore] = useState([]);
    const [openNews, setOpenNews] = useState(false);
    const [newsInfo, setNewsInfo] = useState('');
    const [msgA, setMsgA] = useState([]);
    const [msgM, setMsgM] = useState([]);
    const [messageA, setMessageA] = useState('');
    const [messageM, setMessageM] = useState('');
    const [openSnack, setOpenSnack] = useState(false);
    const [severitySnack, setSeveritySnack] = useState('success');
    const [messageSnack, setMessageSnack] = useState('');
    const [scores, setScores] = useState([]);

    const popSnack = (msg, sev) => {
        setMessageSnack(msg)
        setSeveritySnack(sev)
        setOpenSnack(true)
    }

    const getGroupScores = async (g) => {
        setCurrentGroup(g);
        let user = await Auth.currentUserInfo();
        let t = await CriteriaGetJudgeScore({ idevent: event.idevent, iduser: Number(user.attributes["custom:iduser"]), idgroup: g.idgroup })
        setScore(t.body);
    }

    const getGroups = async () => {
        let t = await EventGetActive({ idevent: event.idevent })
        setGroups(t.body)
    }

    const getNews = async () => {
        let t = await EventListNews({ idevent: event.idevent })
        if (t.body !== null) setAns(t.body)
    }

    const getMsgAse = async () => {
        let t = await EventGetMessages({ idevent: event.idevent })
        if (t.body !== null) setMsgA(t.body)
    }

    const getMsgMen = async () => {
        let t = await EventGetQuestions({ idevent: event.idevent, idgroup: idgroup })
        if (t.body !== null) setMsgM(t.body)
    }

    const sendMsgA = async () => {
        if (messageA.trim() === "") { setMessageA(''); return; }
        let user = await Auth.currentUserInfo();
        let today = new Date()
        let data = {
            idevent: event.idevent,
            text: messageA,
            iduser: Number(user.attributes["custom:iduser"]),
            date: today
        }
        await EventSendMessage(data)
        setMessageA('');
        getMsgAse();
    }

    const sendMsgM = async () => {
        if (messageM.trim() === "") { setMessageM(''); return; }
        let user = await Auth.currentUserInfo();
        let today = new Date()
        let data = {
            idevent: event.idevent,
            idgroup: idgroup,
            text: messageM,
            iduser: Number(user.attributes["custom:iduser"]),
            date: today
        }
        await EventSendQuestion(data)
        setMessageM('');
        getMsgMen();
    }

    const handleViewDocs = async () => {
        let t = await WorkListByGroup({ idevent: event.idevent, idgroup: currentGroup.idgroup });
        setDocs(t.body);
        setOpenDocs(true);
    }

    const changeValue = (index, e) => {
        let clone = JSON.parse(JSON.stringify(score));
        clone[index].value = e.target.value;
        setScore(clone);
    }

    const changeComment = (index, e) => {
        let clone = JSON.parse(JSON.stringify(score));
        clone[index].comment = e.target.value;
        setScore(clone);
    }

    const handleSaveScore = async () => {
        let user = await Auth.currentUserInfo();

        for (let i = 0; i < score.length; i++) {
            if (isNaN(Number(score[i].value))) {
                popSnack('Debe ingresar valores numéricos como puntaje', 'error')
                return;
            }
            if (score[i].value > score[i].max || score[i].value < score[i].min) {
                popSnack('Los puntajes no respetan el mínimo/máximo esperado', 'error')
                return;
            }
        }

        let data = {
            idevent: event.idevent,
            idgroup: currentGroup.idgroup,
            iduser: Number(user.attributes["custom:iduser"]),
            scores: score
        }
        await CriteriaSaveScore(data);
        popSnack('Puntajes guardados', 'success')
    }

    const handleAddNews = async () => {
        let user = await Auth.currentUserInfo();
        let today = new Date()
        let data = { idevent: event.idevent, info: newsInfo, iduser: Number(user.attributes["custom:iduser"]), date: today }
        await EventAddNews(data);
        setOpenNews(false);
        setAns([])
        getNews();
    }

    const handleOpenGrades = async () => {
        setOpenScores(true);
        let t = await CriteriaGetAllScores({ idevent: event.idevent })
        if (t.body !== null) setScores(t.body)
    }

    const handleFinish = async () => {
        let t = await EventFinishHacking({ idevent: event.idevent })
        setOpenFinish(false);
        if (t.body.success) popSnack('Fase de hacking terminada', 'success');
        else popSnack('No se pudo completar la operación', 'error');
    }

    const getRoleName = (i) => {
        if (i === 1) return 'Creador';
        if (i === 2) return 'Administrador';
        if (i === 3) return 'Jurado';
        if (i === 4) return 'Mentor';
        if (i === 5) return 'Asesor';
    }

    const changeNewsInfo = (event) => setNewsInfo(event.target.value);
    const changeMessageA = (event) => setMessageA(event.target.value);
    const changeMessageM = (event) => setMessageM(event.target.value);

    useEffect(() => {
        getNews();
        let interval1 = setInterval(() => getNews(), 30000);
        let interval2 = null
        let interval3 = null

        if (role === 3) getGroups();

        if (role === 1 || role === 2 || role === 5) {
            getMsgAse();
            interval2 = setInterval(() => getMsgAse(), 10000);
        }
        if (role === 4) {
            getMsgMen();
            interval3 = setInterval(() => getMsgMen(), 10000);
        }

        return () => {
            clearInterval(interval1);
            if (role === 1 || role === 2 || role === 5) clearInterval(interval2);
            if (role === 4) clearInterval(interval3);
        }

    }, []);

    return (
        <>
            <Box style={{ backgroundColor: '#f0f0f0', textAlign: 'center' }}>
                <Typography variant="h5">
                    {event.name + ' | ' + getRoleName(role)}
                </Typography>
            </Box>
            <Divider style={{ border: '1px solid #c0c0c0', padding: '1px', backgroundColor: '#f0f0f0' }}></Divider>
            <Grid container spacing={1} justify='space-evenly' alignItems='flex-start' style={{ backgroundColor: '#f0f0f0' }}>
                <Grid item sm={3} component={Box}>
                    <Box style={{ textAlign: 'center' }}>
                        <Typography variant="h5" align='center' style={{ display: 'inline-block' }}>
                            Anuncios
                    </Typography>
                        <Box display={(role === 1 || role === 2) ? 'inline-block' : 'none'}>
                            <IconButton color="primary" variant="contained">
                                <AddIcon onClick={() => { setOpenNews(true); setNewsInfo('') }} />
                            </IconButton>
                        </Box>
                    </Box>
                    <List className={classes.listclass} style={{ border: '1px solid #c0c0c0' }}>
                        {ans.map((a) => (
                            <div key={a.idnews}>
                                <ListItem alignItems="flex-start">
                                    <ListItemText primary={a.info} secondary={a.date} />
                                </ListItem>
                                <Divider />
                            </div>
                        ))}
                    </List>
                </Grid>
                <Grid item sm={3} component={Box} display={(role === 4) ? 'block' : 'none'}>
                    <Typography variant="h5" gutterBottom align='center'>
                        Mentoría
                    </Typography>
                    <List className={classes.listclass} style={{ border: '1px solid #c0c0c0' }}>
                        {msgM.map((a) => (
                            <div key={a.idquestion}>
                                <ListItem alignItems="flex-start" style={{ paddingTop: '0px', paddingBottom: '0px' }}>
                                    <ListItemText primary={a.text} secondary={a.name + ' • ' + a.date} />
                                </ListItem>
                                <Divider />
                            </div>
                        ))}
                    </List>
                    <TextField
                        margin="dense"
                        onChange={changeMessageM}
                        value={messageM}
                        fullWidth
                        variant='outlined'
                        multiline
                        rowsMax={3}
                        InputProps={{ endAdornment: <IconButton onClick={sendMsgM} size='small'><SendIcon /></IconButton> }}
                    />
                </Grid>
                <Grid item sm={3} component={Box} display={(role === 1 || role === 2 || role === 5) ? 'block' : 'none'}>
                    <Typography variant="h5" gutterBottom align='center'>
                        Asesoría
                    </Typography>
                    <List className={classes.listclass} style={{ border: '1px solid #c0c0c0' }}>
                        {msgA.map((a) => (
                            <div key={a.idmessage}>
                                <ListItem alignItems="flex-start" style={{ paddingTop: '0px', paddingBottom: '0px' }}><ListItemText primary={a.text} secondary={a.name + ' • ' + a.date} /></ListItem>
                                <Divider />
                            </div>
                        ))}
                    </List>
                    <TextField
                        margin="dense"
                        onChange={changeMessageA}
                        value={messageA}
                        fullWidth
                        variant='outlined'
                        multiline
                        rowsMax={3}
                        InputProps={{ endAdornment: <IconButton onClick={sendMsgA} size='small'><SendIcon /></IconButton> }}
                    />
                </Grid>
                <Grid container item sm={6} component={Box} display={(role === 3) ? 'inline-flex' : 'none'} spacing={1} justify='space-evenly' alignItems='flex-start'>
                    <Grid item xs={12}>
                        <Typography variant="h5" gutterBottom align='center'>
                            Panel de jurado
                        </Typography>
                    </Grid>
                    <Grid container item spacing={2} style={{ border: '1px solid #c0c0c0', padding: '0px' }}>
                        <Grid item xs={4} style={{ border: '1px solid #c0c0c0', padding: '0px' }}>
                            <List subheader={<ListSubheader>Seleccione un equipo</ListSubheader>} className={classes.root} >
                                {groups.map((g) => {
                                    return (
                                        <ListItem button key={g.idgroup} onClick={() => getGroupScores(g)} style={g.idgroup === currentGroup.idgroup ? { backgroundColor: 'gainsboro' } : {}}>
                                            <ListItemText primary={g.name} />
                                        </ListItem>
                                    )
                                })}
                            </List>
                        </Grid>
                        <Grid item xs={8}>
                            <Typography variant="h6" gutterBottom align='center'>
                                Calificación
                            </Typography>
                            <Box display={(currentGroup.idgroup !== null) ? 'block' : 'none'}>
                                <Box style={{ textAlign: 'center' }}>
                                    <Button variant="contained" size="small" onClick={handleViewDocs} style={{ margin: '8px' }}>Ver documentos</Button>
                                    <Button variant="contained" size="small" color='primary' onClick={handleSaveScore} style={{ margin: '8px' }}>Guardar</Button>
                                </Box>
                                {score.map((c, index) => {
                                    return (
                                        <div key={c.idcriteria} style={{ margin: '1rem' }}>
                                            <Typography variant="body1">{'C' + (index + 1) + '. ' + c.name + ' (mín: ' + c.min + ' | máx: ' + c.max + ')'}</Typography>
                                            <Box display='inline-flex'>
                                                <TextField
                                                    value={c.value}
                                                    onChange={(e) => changeValue(index, e)}
                                                    margin='dense'
                                                    label="Puntaje"
                                                    size='small'
                                                    InputLabelProps={{ shrink: true }}
                                                    variant='outlined'
                                                    style={{ width: '80px', marginBottom: '8px', marginRight: '4px', marginTop: '8px' }}
                                                />
                                                <TextField
                                                    value={c.comment}
                                                    onChange={(e) => changeComment(index, e)}
                                                    margin='dense'
                                                    label="Comentario"
                                                    size='small'
                                                    InputLabelProps={{ shrink: true }}
                                                    variant='outlined'
                                                    multiline
                                                    maxWidth
                                                    style={{ marginBottom: '8px', marginTop: '8px' }}
                                                    rowsMax={4}
                                                />
                                            </Box>
                                        </div >
                                    )
                                })}
                            </Box>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item sm={3} component={Box} display={(role === 1 || role === 2) ? 'block' : 'none'}>
                    <Typography variant="h5" gutterBottom align='center'>
                        Opciones
                    </Typography>
                    <Box style={{ textAlign: 'center' }}>
                        <Button
                            variant="contained"
                            color="primary"
                            className={classes.button}
                            startIcon={<AssignmentIcon />}
                            onClick={() => setOpenFinish(true)}
                        >
                            Terminar etapa de hacking
                    </Button>
                        <Button
                            variant="contained"
                            className={classes.button}
                            startIcon={<SpellcheckIcon />}
                            onClick={handleOpenGrades}
                        >
                            Ver calificaciones
                    </Button>
                    </Box>
                </Grid>
            </Grid>
            <Dialog open={openDocs} onClose={() => setOpenDocs(false)} aria-labelledby="form-dialog-title" fullWidth maxWidth='sm'>
                <DialogTitle id="form-dialog-title">Ver documentos</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        A continuación se muestra una lista de los documentos entregados por el equipo:
                    </DialogContentText>
                    <TableContainer component={Paper}>
                        <Table size="small">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Documento</TableCell>
                                    <TableCell align="right">Descargar</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {docs.map((d) => (
                                    <TableRow key={d.idwork}>
                                        <TableCell component="th" scope="row">
                                            {d.name}
                                        </TableCell>
                                        <TableCell align="right">
                                            <IconButton aria-label="expand row" size="small" onClick={() => window.open(d.file_url)}>
                                                <GetAppIcon />
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </DialogContent>
            </Dialog>
            <Dialog open={openNews} onClose={() => setOpenNews(false)} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Crear anuncio</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Ingrese el mensaje del anuncio. Al hacer click en "Aceptar" el anuncio se publicará y todos los equipos y personal del evento podrán verlo.
                    </DialogContentText>
                    <TextField
                        margin="dense"
                        onChange={changeNewsInfo}
                        value={newsInfo}
                        label="Anuncio"
                        fullWidth
                        multiline
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleAddNews} color="primary">
                        Aceptar
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog open={openScores} onClose={() => setOpenScores(false)} aria-labelledby="scores">
                <DialogTitle id="form-dialog-title">Calificaciones</DialogTitle>
                <DialogContent>
                    <TableContainer component={Paper}>
                        <Table aria-label="collapsible table" size={'small'}>
                            <TableHead>
                                <TableRow>
                                    <TableCell align="left">Equipo</TableCell>
                                    <TableCell align="right">Total</TableCell>
                                    <TableCell align="left">Puntajes</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {scores.map((g) => {
                                    return (
                                        <TableRow key={g.idgroup}>
                                            <TableCell component="th" scope="row">
                                                {g.name}
                                            </TableCell>
                                            <TableCell scope="row">
                                                <b>{g.score}</b>
                                            </TableCell>
                                            {Object.keys(g.scores[0]).map((e, i) => {
                                                return (<TableCell key={i} align="right">{g.scores[0][e]}</TableCell>)
                                            })}
                                        </TableRow>
                                    )
                                })}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </DialogContent>
            </Dialog>
            <Dialog open={openFinish} onClose={() => setOpenFinish(false)} aria-labelledby="finish" fullWidth maxWidth='sm'>
                <DialogTitle id="form-dialog-title">Terminar hacking</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        IMPORTANTE: Al hacer clic en "Aceptar" se dará por terminada la etapa de hacking y los equipos ya no podrán entregar trabajos.
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleFinish} color="primary">
                        Aceptar
                    </Button>
                </DialogActions>
            </Dialog>
            <Snackbar open={openSnack} autoHideDuration={3000} onClose={() => setOpenSnack(false)}>
                <Alert onClose={() => setOpenSnack(false)} severity={severitySnack}>
                    {messageSnack}
                </Alert>
            </Snackbar>
        </>
    )
};

export default During;