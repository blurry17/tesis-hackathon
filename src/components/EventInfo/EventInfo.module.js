import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    paper: {
        margin: theme.spacing(2),
        flexDirection: 'column',
        alignItems: 'center',
        textAlign: 'left'
    },
    info: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 300,
    },
    btns: {
        margin: theme.spacing(2)
    }
}));