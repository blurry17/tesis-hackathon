import React, { useState } from 'react';
import { useStyles } from './EventInfo.module';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { Auth } from 'aws-amplify';
import { useHistory } from "react-router-dom";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { TeamsGetCreatedSimple, EventEnrollTeam, EventEnrollUser, EventDownloadRules } from '../../api/services';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import GetAppIcon from '@material-ui/icons/GetApp';
import PersonIcon from '@material-ui/icons/Person';
import PeopleIcon from '@material-ui/icons/People';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const EventInfo = (props) => {
    const event = props.history.location.state;
    const classes = useStyles();
    const history = useHistory();
    const [openG, setOpenG] = useState(false);
    const [teams, setTeams] = useState([]);
    const [teamId, setTeamId] = useState(0);
    const [success, setSuccess] = useState(true);
    const [alertmsg, setAlertmsg] = useState('');
    const [openAlert, setOpenAlert] = useState(false)

    const handleCloseG = () => setOpenG(false);
    const handleChange = (e) => setTeamId(e.target.value);

    const handleClickPersonEnroll = async () => {
        let user = await Auth.currentUserInfo();
        if (user) {
            let data = {
                iduser: Number(user.attributes["custom:iduser"]),
                idevent: event.idevent
            }
            let t = await EventEnrollUser(data);
            setSuccess(t.body.success)
            setAlertmsg(t.message)
            setOpenAlert(true)
        } else history.push('/login')
    }

    const handleClickGroupEnroll = async () => {
        let user = await Auth.currentUserInfo();
        if (user) {
            setOpenG(true);
            let t = await TeamsGetCreatedSimple({ idUser: Number(user.attributes["custom:iduser"]) });
            setTeams(t.body)
        } else history.push('/login')
    }

    const handleEnrollTeam = async () => {
        let data = {
            idgroup: teamId,
            idevent: event.idevent
        }
        await EventEnrollTeam(data);
        setOpenG(false);
    }

    const handleClickDownloadRules = async () => {
        let t = await EventDownloadRules({ idevent: event.idevent });
        window.open(t.body.file_url);
    }

    const getEmbed = (url) => {
        if (url === null) return null;
        if (url.includes("youtube.com")) return url.replace("watch?v=", "embed/");
        if (url.includes("youtu.be")) return url.replace("youtu.be/", "www.youtube.com/embed/");
        return null;
    }

    return (
        <>
            <Container component="main" maxWidth="md" >
                <Box className={classes.paper}>
                    <Grid container style={{ display: 'block' }}>
                        <Typography variant="h2">
                            {event.name}
                        </Typography>
                        <Typography variant="h6">
                            Inscríbete hasta: {event.registration_close_date}
                        </Typography>
                        <Typography variant="h6">
                            Fechas del evento: {event.start_date} hasta {event.end_date}
                        </Typography>
                        <Box display={Boolean(event.location) ? 'block' : 'none'}>
                            <Typography variant="h6">
                                Lugar del evento: {event.location}
                            </Typography>
                        </Box>
                        <Box display={Boolean(event.videolink) ? 'block' : 'none'} style={{ textAlign: 'center' }}>
                            <iframe title="Video promocional" width="560" height="315" src={getEmbed(event.videolink)} frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullscreen></iframe>
                        </Box>
                        {event.long_desc.split('\n').map((item, i) => { return <Typography variant="body1" key={i} style={{ textAlign: 'justify', textJustify: 'auto' }} gutterBottom>{item}</Typography> })}
                        <Box display={(event.registration_type === 1 || event.registration_type === 3) ? 'inline' : 'none'}>
                            <Button
                                variant="contained"
                                color="primary"
                                className={classes.btns}
                                startIcon={<PersonIcon />}
                                onClick={handleClickPersonEnroll}
                            >
                                Inscripción individual
                            </Button>
                        </Box>
                        <Box display={(event.registration_type === 2 || event.registration_type === 3) ? 'inline' : 'none'}>
                            <Button
                                variant="contained"
                                color="primary"
                                className={classes.btns}
                                startIcon={<PeopleIcon />}
                                onClick={handleClickGroupEnroll}
                            >
                                Inscripción grupal
                            </Button>
                        </Box>
                        <Box display='inline'>
                            <Button
                                variant="contained"
                                className={classes.btns}
                                startIcon={<GetAppIcon />}
                                onClick={handleClickDownloadRules}
                            >
                                Descargar bases
                            </Button>
                        </Box>
                    </Grid>
                </Box>
            </Container>
            <Dialog open={openG} onClose={handleCloseG} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Inscribir equipo</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        {'IMPORTANTE: Este evento admite equipos de ' + event.min_group + ' integrantes como mínimo y ' + event.max_group + ' integrantes como máximo. Seleccione el equipo que deseas inscribir:'}
                    </DialogContentText>
                    <FormControl variant="outlined" className={classes.formControl}>
                        <InputLabel id="equipo-select">Equipo</InputLabel>
                        <Select
                            labelId="demo-simple-select-outlined-label"
                            id="demo-simple-select-outlined"
                            value={teamId}
                            onChange={handleChange}
                            label="Equipo"
                        >
                            <MenuItem value={0}>
                                <em>Seleccionar equipo</em>
                            </MenuItem>
                            {(teams.map((t) => {
                                return (
                                    <MenuItem key={t.idgroup} value={t.idgroup}>{t.name}</MenuItem>
                                )
                            }))}
                        </Select>
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleEnrollTeam} color="primary" disabled={teamId === 0}>
                        Inscribir equipo
                    </Button>
                </DialogActions>
            </Dialog>
            <Snackbar open={openAlert} autoHideDuration={3000} onClose={() => setOpenAlert(false)}>
                <Alert onClose={() => setOpenAlert(false)} severity={success ? 'success' : 'error'}>
                    {alertmsg}
                </Alert>
            </Snackbar>
        </>
    );
}

export default EventInfo;