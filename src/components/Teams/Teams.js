import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import { Auth } from 'aws-amplify';
import { NotificationJoinTeam, TeamsGetCreated, TeamsCreate, TeamsGetOther, TeamDelete } from '../../api/services';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import { useStyles } from './Teams.module';

const Teams = () => {
    const classes = useStyles();
    const [teams, setTeams] = useState([])
    const [otherTeams, setOtherTeams] = useState([])
    const [openCreate, setOpenCreate] = useState(false);
    const [openAdd, setOpenAdd] = useState(false);
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [current, setCurrent] = useState(null);

    const handleCreateTeam = async () => {
        let user = await Auth.currentUserInfo();
        await TeamsCreate({ idUser: Number(user.attributes["custom:iduser"]), name: name })
        handleCloseCreate();
        getCreatedTeams();
    }

    const getCreatedTeams = async () => {
        let user = await Auth.currentUserInfo();
        let t = await TeamsGetCreated({ idUser: Number(user.attributes["custom:iduser"]) });
        setTeams(t.body.teams)
    }

    const getOtherTeams = async () => {
        let user = await Auth.currentUserInfo();
        let t = await TeamsGetOther({ idUser: Number(user.attributes["custom:iduser"]) });
        setOtherTeams(t.body.teams)
    }

    const handleInvite = async () => {
        let user = await Auth.currentUserInfo();
        await NotificationJoinTeam({ idgroup: current.idgroup, email: email, iduser: Number(user.attributes["custom:iduser"]) })
        setEmail('');
        handleCloseAdd();
    }

    const handleClickOpenCreate = () => setOpenCreate(true);
    const handleClickOpenAdd = (t) => { setOpenAdd(true); setCurrent(t); }
    const handleCloseCreate = () => setOpenCreate(false);
    const handleCloseAdd = () => setOpenAdd(false);

    const handleClickDelete = async (g, index) => {
        let aux = JSON.parse(JSON.stringify(teams));
        aux.splice(index, 1);
        setTeams(aux)
        await TeamDelete({ idgroup: g.idgroup })
    };

    const changeName = (event) => setName(event.target.value)
    const changeEmail = (event) => setEmail(event.target.value)

    useEffect(() => {
        getCreatedTeams();
        getOtherTeams();
    }, []);

    return (
        <Container component='main' maxWidth='sm'>
            <div className={classes.paper}>
                <Typography variant='h3'>
                    Mis equipos
                </Typography>
                <Button variant="contained" onClick={handleClickOpenCreate}>Crear equipo</Button>
                <Grid maxWidth>
                    {(teams.map((t, index) => {
                        return (
                            <Card key={t.idgroup} style={{ margin: '1rem' }}>
                                <CardHeader
                                    style={{ paddingBottom: '0px' }}
                                    action={
                                        <CardActions>
                                            <Tooltip title="Agregar integrante">
                                                <IconButton onClick={() => handleClickOpenAdd(t)} disabled={t.status === 2}>
                                                    <PersonAddIcon />
                                                </IconButton>
                                            </Tooltip>
                                            <Tooltip title="Eliminar equipo">
                                                <IconButton onClick={() => handleClickDelete(t, index)}>
                                                    <DeleteOutlineIcon />
                                                </IconButton>
                                            </Tooltip>
                                        </CardActions>
                                    }
                                    title={t.name}
                                />
                                <CardContent style={{ paddingTop: '0px' }}>
                                    <Typography variant="body2">
                                        Integrantes:
                                    </Typography>
                                    {t.members.map((m) => {
                                        return (
                                            <Typography variant="body2" key={m.iduser}>
                                                {m.name}
                                            </Typography>
                                        )
                                    })}

                                </CardContent>
                            </Card>
                        )
                    }))}
                    {(otherTeams.map((t) => {
                        return (
                            <Card key={t.idgroup}>
                                <CardHeader
                                    style={{ paddingBottom: '0px' }}
                                    title={t.name}
                                />
                                <CardContent style={{ paddingTop: '0px' }}>
                                    <Typography variant="body2">
                                        Integrantes:
                                    </Typography>
                                    {t.members.map((m) => {
                                        return (
                                            <Typography variant="body2">
                                                {m.name}
                                            </Typography>
                                        )
                                    })}

                                </CardContent>
                            </Card>
                        )
                    }))}
                </Grid>
            </div>
            <Dialog open={openCreate} onClose={handleCloseCreate} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Crear equipo</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Ingresa el nombre del equipo, luego podrás invitar a otros usuarios a unirse.
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        onChange={changeName}
                        value={name}
                        label="Nombre del equpo"
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCreateTeam} color="primary">
                        Crear equipo
                        </Button>
                </DialogActions>
            </Dialog>
            <Dialog open={openAdd} onClose={handleCloseAdd} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Agregar integrante</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Ingresa el correo electrónico del usuario al que quieres invitar (debe estar registrado con ese correo)
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="email"
                        onChange={changeEmail}
                        value={email}
                        label="Correo electrónico"
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleInvite} color="primary">
                        Invitar
                    </Button>
                </DialogActions>
            </Dialog>
        </Container>
    );
}

export default Teams;