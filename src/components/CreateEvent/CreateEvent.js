import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import { useStyles } from './CreateEvent.module';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardDatePicker, TimePicker } from '@material-ui/pickers';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import Slider from '@material-ui/core/Slider';
import Box from '@material-ui/core/Box';
import Switch from '@material-ui/core/Switch';
import Divider from '@material-ui/core/Divider';
import Alert from '@material-ui/lab/Alert';
import Dropzone from '../../utils/UploadFiles/Dropzone';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormLabel from '@material-ui/core/FormLabel';
import { Auth } from 'aws-amplify';
import { EventCreate, EventUploadRules, EventUploadImage } from '../../api/services';
import { useHistory } from "react-router-dom";

Date.prototype.addDays = function (days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

const CreateEvent = () => {
    const classes = useStyles();
    const history = useHistory()
    const today = new Date();
    const multiple = false;
    const [nGroups, setNGroups] = useState([4, 5]);
    const [startDate, setStartDate] = useState(today.addDays(7))
    const [endDate, setEndDate] = useState(today.addDays(8))
    const [startTime, setStartTime] = useState(new Date(2020, 1, 1, 9))
    const [endTime, setEndTime] = useState(new Date(2020, 1, 1, 18))
    const [openDate, setOpenDate] = useState(new Date())
    const [closeDate, setCloseDate] = useState(today.addDays(1))
    const [openNow, setOpenNow] = useState(true);
    const [creating, setCreating] = useState(false)
    const [files, setFiles] = useState([]);
    const [images, setImages] = useState([]);
    const [errors, setErrors] = useState({
        no_name: false,
        bad_dates_main_event: false,
        bad_dates_enroll: false
    });
    const [input, setInput] = useState({
        name: '',
        shortDesc: '',
        longDesc: '',
        regType: 1,
        minGroup: 4,
        maxGroup: 5,
        location: '',
        video: ''
    });

    const handleChangeSlider = (event, newValue) => setNGroups(newValue)
    const handleInputChange = (event) => setInput({ ...input, [event.target.id]: event.target.value })
    const handleRadioChange = (event) => setInput({ ...input, [event.target.name]: Number(event.target.value) })
    const handleStartDateChange = (date) => setStartDate(date)
    const handleEndDateChange = (date) => setEndDate(date)
    const handleOpenDateChange = (date) => setOpenDate(date)
    const handleCloseDateChange = (date) => setCloseDate(date)
    const handleStartTimeChange = (date) => setStartTime(date)
    const handleEndTimeChange = (date) => setEndTime(date)
    const handleChangeOpenNow = () => { setOpenNow(!openNow); setOpenDate(today) }

    const validateInputs = () => {
        if (!input.name.trim()) return { no_name: true };

        let sd = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(), startTime.getHours(), startTime.getMinutes(), 0, 0);
        let ed = new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate(), endTime.getHours(), endTime.getMinutes(), 0, 0);
        if (sd.getTime() > ed.getTime()) return { bad_dates_main_event: true };

        let od = openNow ? today : new Date(openDate.getFullYear(), openDate.getMonth(), openDate.getDate(), 0, 0, 0, 1)
        let cd = new Date(closeDate.getFullYear(), closeDate.getMonth(), closeDate.getDate(), 23, 59, 59, 999);
        if (od.getTime() > cd.getTime()) return { bad_dates_enroll: true };

        return null;
    }

    const clearErrorState = async () => {
        setErrors({
            no_name: false,
            bad_dates_main_event: false,
            bad_dates_enroll: false
        })
    }

    const handleClickCreate = async event => {
        event.preventDefault();
        setCreating(true);
        await clearErrorState();
        const error = validateInputs();
        if (error) {
            setErrors({ ...errors, ...error });
            setCreating(false);
            return;
        }

        try {
            let user = await Auth.currentUserInfo();
            let data = {
                idUser: Number(user.attributes["custom:iduser"]),
                name: input.name,
                shortDesc: input.shortDesc,
                longDesc: input.longDesc,
                startDate: new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(), startTime.getHours(), startTime.getMinutes(), 0, 0),
                endDate: new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate(), endTime.getHours(), endTime.getMinutes(), 0, 0),
                videolink: input.video,
                createdOn: today,
                openDate: openNow ? today : new Date(openDate.getFullYear(), openDate.getMonth(), openDate.getDate(), 0, 0, 0, 0),
                closeDate: new Date(closeDate.getFullYear(), closeDate.getMonth(), closeDate.getDate(), 23, 59, 59, 0),
                regType: input.regType,
                minGroup: Math.min(...nGroups),
                maxGroup: Math.max(...nGroups),
                location: input.location,
                user_last_modified: Number(user.attributes["custom:iduser"])
            }
            let res = await EventCreate(data);
            if (res.body === null) { setCreating(false); return; }
            else {
                if (files.length !== 0) {
                    let data = { idevent: res.body.idEvent, iduser: Number(user.attributes["custom:iduser"]) }
                    await EventUploadRules(data, files[0]);
                }
                if (images.length !== 0) {
                    let data = { idevent: res.body.idEvent, iduser: Number(user.attributes["custom:iduser"]) }
                    await EventUploadImage(data, images[0]);
                }
                history.push('/');
            }
        } catch (e) {
            console.dir(e)
        }
    }

    const onFilesAdded = (fileArray) => {
        let encontrado = false;
        if (files.length > 0 && !multiple) return;
        if (files === []) setFiles(prevFiles => files.concat(fileArray));
        else {
            files.map((x) => {
                if (x.name === fileArray[0].name) encontrado = true;
                return x;
            })
            if (!encontrado) setFiles(prevFiles => files.concat(fileArray));
        }
    }

    const onFilesAdded2 = (fileArray) => {
        let encontrado = false;
        if (images.length > 0 && !multiple) return;
        if (images === []) setImages(prevFiles => files.concat(fileArray));
        else {
            images.map((x) => {
                if (x.name === fileArray[0].name) encontrado = true;
                return x;
            })
            if (!encontrado) setImages(prevFiles => images.concat(fileArray));
        }
    }

    const formatBytes = (bytes, decimals = 2) => {
        if (bytes === 0) return '0 Bytes';
        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        const i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    const eliminarArchivo = (data, option) => {
        let aux = option === 1 ? [...files] : [...images]
        aux.map((x, index) => {
            if (x.name === data) aux.splice(index, 1);
            return x;
        })
        option === 1 ? setFiles(aux) : setImages(aux);
    }

    return (
        <Container component="main" maxWidth="sm">
            <CssBaseline />
            <div className={classes.paper}>
                <Typography variant="h4">
                    Crea tu evento
                    </Typography>
                <form className={classes.form} noValidate>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <Grid container>
                            <Typography variant="h6">
                                Información básica
                            </Typography>
                            <Grid item sm={12}>
                                <TextField
                                    InputLabelProps={{ shrink: true }}
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="name"
                                    label="Nombre del evento"
                                    name="name"
                                    autoComplete="name"
                                    helperText="¡Piensa en un nombre divertido y fácil de recordar!"
                                    autoFocus
                                    value={input.name}
                                    onChange={handleInputChange}
                                />
                            </Grid>
                            <Grid item sm={12}>
                                <TextField
                                    multiline
                                    InputLabelProps={{ shrink: true }}
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    name="shortDesc"
                                    label="Descripción breve"
                                    id="shortDesc"
                                    helperText="Resume tu evento en un par de oraciones"
                                    value={input.shortDesc}
                                    onChange={handleInputChange}
                                    inputProps={{ maxLength: 200 }}
                                />
                            </Grid>
                            <Grid item sm={12}>
                                <TextField
                                    multiline
                                    rows={3}
                                    rowsMax={10}
                                    InputLabelProps={{ shrink: true }}
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    name="longDesc"
                                    label="Información general"
                                    type="longDesc"
                                    id="longDesc"
                                    helperText="Explica todos los detalles del evento"
                                    value={input.longDesc}
                                    onChange={handleInputChange}
                                    inputProps={{ maxLength: 10000 }}
                                />
                            </Grid>
                            <Grid item sm={6}>
                                <KeyboardDatePicker
                                    autoOk
                                    inputVariant="outlined"
                                    margin="normal"
                                    id="startDate"
                                    label="Fecha de INICIO del evento"
                                    format="dd/MM/yyyy"
                                    value={startDate}
                                    onChange={handleStartDateChange}
                                    KeyboardButtonProps={{
                                        'aria-label': 'change date',
                                    }}
                                />
                            </Grid>
                            <Grid item sm={6}>
                                <TimePicker
                                    autoOk
                                    inputVariant="outlined"
                                    margin="normal"
                                    id="startTime"
                                    label="Hora de INICIO del evento"
                                    value={startTime}
                                    onChange={handleStartTimeChange}
                                    KeyboardButtonProps={{
                                        'aria-label': 'change time',
                                    }}
                                />
                            </Grid>
                            <Grid item sm={6}>
                                <KeyboardDatePicker
                                    autoOk
                                    inputVariant="outlined"
                                    margin="normal"
                                    id="endDate"
                                    label="Fecha de FIN del evento"
                                    format="dd/MM/yyyy"
                                    value={endDate}
                                    onChange={handleEndDateChange}
                                    KeyboardButtonProps={{
                                        'aria-label': 'change date',
                                    }}
                                />
                            </Grid>
                            <Grid item sm={6}>
                                <TimePicker
                                    autoOk
                                    inputVariant="outlined"
                                    margin="normal"
                                    id="endTime"
                                    label="Hora de FIN del evento"
                                    value={endTime}
                                    onChange={handleEndTimeChange}
                                    KeyboardButtonProps={{
                                        'aria-label': 'change time',
                                    }}
                                />
                            </Grid>
                            <Divider />
                            <Typography variant="h6">
                                Inscripciones
                            </Typography>
                            <Grid item sm={12}>
                                <FormControlLabel
                                    control={
                                        <Switch
                                            checked={openNow}
                                            onChange={handleChangeOpenNow}
                                            name="openNow"
                                            color="primary"
                                        />
                                    }
                                    label="Abrir las inscripciones al crear el evento"
                                />
                            </Grid>
                            <Grid component={Box} item sm={6}>
                                <KeyboardDatePicker
                                    autoOk
                                    inputVariant="outlined"
                                    margin="normal"
                                    id="date-picker-dialog"
                                    label="Fecha de INICIO de inscripciones"
                                    format="dd/MM/yyyy"
                                    value={openDate}
                                    onChange={handleOpenDateChange}
                                    disabled={openNow ? true : false}
                                />
                            </Grid>
                            <Grid component={Box} item sm={6}>
                                <KeyboardDatePicker
                                    autoOk
                                    inputVariant="outlined"
                                    margin="normal"
                                    id="date-picker-dialog"
                                    label="Fecha de CIERRE de inscripciones"
                                    format="dd/MM/yyyy"
                                    value={closeDate}
                                    onChange={handleCloseDateChange}
                                />
                            </Grid>
                            <Grid item sm={12}>
                                <FormControl component="fieldset">
                                    <FormLabel component="legend">¿Cómo se pueden inscribir a este evento?</FormLabel>
                                    <RadioGroup aria-label="regType" name="regType" value={input.regType} id="regType" onChange={handleRadioChange}>
                                        <FormControlLabel value={1} control={<Radio color="primary" />} label="Como individuos (que luego serán agrupados)" />
                                        <FormControlLabel value={2} control={<Radio color="primary" />} label="Solo como grupos" />
                                        <FormControlLabel value={3} control={<Radio color="primary" />} label="Como individuos y grupos" />
                                    </RadioGroup>
                                </FormControl>
                            </Grid>
                            <Grid item sm={12}>
                                <Typography id="range-slider" gutterBottom>
                                    ¿De cuántas personas puede ser un grupo? Seleccione un rango
                                </Typography>
                                <Slider
                                    className={classes.slider}
                                    step={1}
                                    marks
                                    min={2}
                                    max={10}
                                    value={nGroups}
                                    onChange={handleChangeSlider}
                                    valueLabelDisplay="on"
                                    aria-labelledby="range-slider"
                                    style={{ paddingTop: '50px' }}

                                />
                            </Grid>
                            <Typography variant="h6">
                                Detalles del evento
                            </Typography>
                            <Grid item sm={12}>
                                <TextField
                                    InputLabelProps={{ shrink: true }}
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="location"
                                    label="Ubicación del evento"
                                    name="location"
                                    value={input.location}
                                    onChange={handleInputChange}
                                    autoComplete="location"
                                />
                            </Grid>
                            <Grid item sm={12}>
                                <TextField
                                    InputLabelProps={{ shrink: true }}
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="video"
                                    label="Video promocional en YouTube"
                                    helperText="Ejemplos: https://www.youtube.com/watch?v=wJWksPWDKOc ó https://youtu.be/wJWksPWDKOc"
                                    name="video"
                                    value={input.video}
                                    onChange={handleInputChange}
                                />
                            </Grid>
                            <Grid item sm={12}>
                                <Typography>Documento de bases del concurso (1 archivo, máx 3MB)</Typography>
                                <Dropzone
                                    onFilesAdded={onFilesAdded}
                                    disabled={false}
                                    image={false}
                                    multiple={false}
                                />
                                <div className={classes.files}>
                                    {files.map(file => {
                                        return (
                                            <Alert key={file.name}
                                                onClose={() => eliminarArchivo(file.name, 1)}>{file.name} - {formatBytes(file.size)}</Alert>
                                        )
                                    })}
                                </div>
                            </Grid>
                            <Grid item sm={12}>
                                <Typography>Imagen promocional (1 archivo, máx 10MB)</Typography>
                                <Dropzone
                                    onFilesAdded={onFilesAdded2}
                                    disabled={false}
                                    image={true}
                                    multiple={false}
                                    style={{ heigth: '30px' }}
                                />
                                <div className={classes.files}>
                                    {images.map(file => {
                                        return (
                                            <Alert key={file.name}
                                                onClose={() => eliminarArchivo(file.name, 2)}>{file.name} - {formatBytes(file.size)}</Alert>
                                        )
                                    })}
                                </div>
                            </Grid>
                            <Divider />
                        </Grid>
                    </MuiPickersUtilsProvider>
                    <Box display={errors.no_name === false ? 'none' : 'block'}>
                        <Alert variant="filled" severity="error" >
                            El evento debe tener un nombre
                        </Alert>
                    </Box>
                    <Box display={errors.bad_dates_main_event === false ? 'none' : 'block'}>
                        <Alert variant="filled" severity="error" >
                            La fechas de fin del evento es anterior a la fecha de inicio del evento
                        </Alert>
                    </Box>
                    <Box display={errors.bad_dates_enroll === false ? 'none' : 'block'}>
                        <Alert variant="filled" severity="error" >
                            La fecha de fin de inscripciones es anterior a la fecha de inicio de inscripciones
                        </Alert>
                    </Box>

                    <Button
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        onClick={handleClickCreate}
                        disabled={creating}
                    >
                        Crear evento
                        </Button>
                </form>
            </div>
        </Container>

    );
}

export default CreateEvent;