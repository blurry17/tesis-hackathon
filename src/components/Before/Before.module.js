import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    menu: {
        padding: theme.spacing(1),
        backgroundColor: '#f0f0f0',
        overflowX: 'hidden',
        overflowY: 'hidden'
    },
}));