import React, { useState } from 'react';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { useStyles } from './Before.module';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Typography from '@material-ui/core/Typography';
import { useRouteMatch, Switch, Route, useHistory } from "react-router-dom";
import Enroll from '../Enroll/Enroll';
import Staff from '../Staff/Staff';
import Tasks from '../Tasks/Tasks';
import Schedule from '../Schedule/Schedule'
import SettingsIcon from '@material-ui/icons/Settings';
import PeopleIcon from '@material-ui/icons/People';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import ScheduleIcon from '@material-ui/icons/Schedule';
import SpellcheckIcon from '@material-ui/icons/Spellcheck';
import ListAltIcon from '@material-ui/icons/ListAlt';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import Grading from '../Grading/Grading';
import General from '../General/General';
import Sponsors from '../Sponsors/Sponsors';
import { Box } from '@material-ui/core';

const Before = (props) => {
    const classes = useStyles();
    const event = props.history.location.state.event;
    const role = props.history.location.state.role;
    let { path, url } = useRouteMatch();
    const history = useHistory();
    const [currentItem, setCurrentItem] = useState(0);

    const handleClickGeneral = () => {
        setCurrentItem(1);
        history.push(`${url}/general`, { event: event, role: role });
    }
    const handleClickEnroll = () => {
        setCurrentItem(2);
        history.push(`${url}/enrollment`, { event: event, role: role });
    }
    const handleClickStaff = () => {
        setCurrentItem(3);
        history.push(`${url}/staff`, { event: event, role: role });
    }
    const handleClickSchedule = () => {
        setCurrentItem(4);
        history.push(`${url}/schedule`, { event: event, role: role });
    }
    const handleClickGrading = () => {
        setCurrentItem(5);
        history.push(`${url}/grading`, { event: event, role: role });
    }
    const handleClickTasks = () => {
        setCurrentItem(6);
        history.push(`${url}/tasks`, { event: event, role: role });
    }
    const handleClickSponsors = () => {
        setCurrentItem(7);
        history.push(`${url}/sponsors`, { event: event, role: role });
    }

    return (
        <>
            <Box style={{ padding: '0.5rem', backgroundColor: '#f0f0f0' }}>
                <Typography variant="h5" square>
                    {event.name}
                </Typography>
            </Box>
            <Grid container className={classes.menu} spacing={2}>
                <Grid item xs={2}>
                    <Paper style={{ maxWidth: '15rem' }}>
                        <MenuList>
                            <Box display={(role === 1 || role === 2) ? 'block' : 'none'}>
                                <MenuItem onClick={handleClickGeneral} style={currentItem === 1 ? { backgroundColor: 'gainsboro' } : {}}>
                                    <ListItemIcon style={{ minWidth: '30px' }}><SettingsIcon fontSize="small" /></ListItemIcon>
                                    <Typography variant="inherit">General</Typography>
                                </MenuItem>
                            </Box>
                            <Box display={(role === 1 || role === 2) ? 'block' : 'none'}>
                                <MenuItem onClick={handleClickEnroll} style={currentItem === 2 ? { backgroundColor: 'gainsboro' } : {}}>
                                    <ListItemIcon style={{ minWidth: '30px' }}><PeopleIcon fontSize="small" /> </ListItemIcon>
                                    <Typography variant="inherit">Inscripciones</Typography>
                                </MenuItem>
                            </Box>
                            <Box display={(role === 1 || role === 2) ? 'block' : 'none'}>
                                <MenuItem onClick={handleClickStaff} style={currentItem === 3 ? { backgroundColor: 'gainsboro' } : {}}>
                                    <ListItemIcon style={{ minWidth: '30px' }}><AssignmentIndIcon fontSize="small" /></ListItemIcon>
                                    <Typography variant="inherit">Personal</Typography>
                                </MenuItem>
                            </Box>
                            <Box display={(role === 1 || role === 2) ? 'block' : 'none'}>
                                <MenuItem onClick={handleClickSchedule} style={currentItem === 4 ? { backgroundColor: 'gainsboro' } : {}}>
                                    <ListItemIcon style={{ minWidth: '30px' }}><ScheduleIcon fontSize="small" /></ListItemIcon>
                                    <Typography variant="inherit">Cronograma</Typography>
                                </MenuItem>
                            </Box>
                            <Box display={(role === 1 || role === 2 || role === 3) ? 'block' : 'none'}>
                                <MenuItem onClick={handleClickGrading} style={currentItem === 5 ? { backgroundColor: 'gainsboro' } : {}}>
                                    <ListItemIcon style={{ minWidth: '30px' }}><SpellcheckIcon fontSize="small" /></ListItemIcon>
                                    <Typography variant="inherit">Calificación</Typography>
                                </MenuItem>
                            </Box>
                            <Box display={(role === 1 || role === 2) ? 'block' : 'none'}>
                                <MenuItem onClick={handleClickTasks} style={currentItem === 6 ? { backgroundColor: 'gainsboro' } : {}}>
                                    <ListItemIcon style={{ minWidth: '30px' }}><ListAltIcon fontSize="small" /></ListItemIcon>
                                    <Typography variant="inherit">Tareas y compras</Typography>
                                </MenuItem>
                            </Box>
                            <Box display={(role === 1 || role === 2) ? 'block' : 'none'}>
                                <MenuItem onClick={handleClickSponsors} style={currentItem === 7 ? { backgroundColor: 'gainsboro' } : {}}>
                                    <ListItemIcon style={{ minWidth: '30px' }}><MonetizationOnIcon fontSize="small" /></ListItemIcon>
                                    <Typography variant="inherit">Sponsors</Typography>
                                </MenuItem>
                            </Box>
                        </MenuList>
                    </Paper>
                </Grid>
                <Grid item xs={10} style={{ maxWidth: '50rem' }}>
                    <Switch>
                        <Route exact path={`${path}/general`} component={General} />
                        <Route exact path={`${path}/enrollment`} component={Enroll} />
                        <Route exact path={`${path}/staff`} component={Staff} />
                        <Route exact path={`${path}/schedule`} component={Schedule} />
                        <Route exact path={`${path}/tasks`} component={Tasks} />
                        <Route exact path={`${path}/grading`} component={Grading} />
                        <Route exact path={`${path}/sponsors`} component={Sponsors} />
                    </Switch>
                </Grid>
            </Grid>
        </>
    );
}

export default Before;