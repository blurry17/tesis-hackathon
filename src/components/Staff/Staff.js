import React, { useEffect, useState } from 'react';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import SpeakerGroupIcon from '@material-ui/icons/SpeakerGroup';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import PeopleOutlineIcon from '@material-ui/icons/PeopleOutline';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { useStyles } from './Staff.module';
import { Auth } from 'aws-amplify';
import { NotificationJoinEvent, StaffList, StaffListRoles, StaffAssignRole, EventGetAccepted, StaffAssignTeam } from '../../api/services';
import { Tooltip } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import Snackbar from '@material-ui/core/Snackbar';

const Staff = (props) => {
    const event = props.history.location.state.event;
    const role = props.history.location.state.role;
    const classes = useStyles();
    const [staff, setStaff] = useState([]);
    const [openInvite, setOpenInvite] = useState(false);
    const [email, setEmail] = useState('');
    const [openRoles, setOpenRoles] = useState(false);
    const [openTeams, setOpenTeams] = useState(false);
    const [idrole, setIdrole] = useState(0);
    const [idgroup, setIdgroup] = useState(0);
    const [roles, setRoles] = useState([]);
    const [teams, setTeams] = useState([]);
    const [currentUser, setCurrentUser] = useState(null);
    const [openSnack, setOpenSnack] = useState(false);
    const [severitySnack, setSeveritySnack] = useState('success');
    const [messageSnack, setMessageSnack] = useState('');

    const popSnack = (msg, sev) => {
        setMessageSnack(msg)
        setSeveritySnack(sev)
        setOpenSnack(true)
    }

    const getStaff = async () => {
        let t = await StaffList({ idevent: event.idevent });
        setStaff(t.body)
    }

    const handleAssignRole = async () => {
        await StaffAssignRole({ idevent: event.idevent, idtype: idrole, iduser: currentUser });
        setOpenRoles(false)
        setStaff([])
        getStaff();
        popSnack('Rol asignado', 'success');
    }

    const handleAssignTeam = async () => {
        await StaffAssignTeam({ idevent: event.idevent, idgroup: idgroup, iduser: currentUser });
        setOpenTeams(false)
        setStaff([])
        getStaff();
        popSnack('Equipo asignado', 'success');
    }

    const handleInvite = async () => {
        let user = await Auth.currentUserInfo();
        await NotificationJoinEvent({ idevent: event.idevent, email: email, iduser: Number(user.attributes["custom:iduser"]) });
        setOpenInvite(false)
        popSnack('Invitación enviada', 'success');
    }

    const handleOpenAssign = async (u) => {
        setOpenTeams(true);
        setCurrentUser(u.iduser)
        let t = await EventGetAccepted({ idevent: event.idevent });
        setTeams(t.body);
    }

    const handleOpenRoles = async (u) => {
        setOpenRoles(true);
        setCurrentUser(u.iduser)
        let t = await StaffListRoles();
        setRoles(t.body);
        setIdrole(0);
    }

    const handleClickInvite = () => { setOpenInvite(true); setEmail(''); }
    const handleCloseInvite = () => setOpenInvite(false);
    const handleCloseRoles = () => setOpenRoles(false);
    const changeEmail = (event) => setEmail(event.target.value);
    const handleChangeRole = (e) => setIdrole(e.target.value);
    const handleChangeTeam = (e) => setIdgroup(e.target.value);

    useEffect(() => {
        getStaff();
    }, []);


    return (
        <>
            <Typography variant="h6" gutterBottom>
                Lista del personal
            </Typography>
            <Button variant="contained" color="primary" onClick={handleClickInvite}>Agregar personal</Button>
            <TableContainer component={Paper} style={{ marginTop: '1rem' }}>
                <Table aria-label="collapsible table" size={'small'}>
                    <TableHead>
                        <TableRow>
                            <TableCell align="left">Nombre</TableCell>
                            <TableCell align="right">Rol</TableCell>
                            <TableCell align="right">Grupo asignado</TableCell>
                            <TableCell align="right">Acciones</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {staff.map((g) => (
                            <TableRow key={g.iduser}>
                                <TableCell component="th" scope="row">
                                    {g.name}
                                </TableCell>
                                <TableCell align="right">{g.role}</TableCell>
                                <TableCell align="right">{g.assigned_group}</TableCell>
                                <TableCell align="right">
                                    <Box display={g.type === 4 && g.assigned_group === null ? 'block' : 'none'}>
                                        <Tooltip title="Asignar equipo">
                                            <IconButton aria-label="expand row" size="small" onClick={() => handleOpenAssign(g)} disabled={role === 3 | role === 4 | role === 5}>
                                                <PeopleOutlineIcon />
                                            </IconButton>
                                        </Tooltip>
                                    </Box>
                                    <Box display={g.role === null ? 'block' : 'none'}>
                                        <Tooltip title="Asignar rol">
                                            <IconButton aria-label="expand row" size="small" onClick={() => handleOpenRoles(g)} disabled={role === 3 | role === 4 | role === 5}>
                                                <SpeakerGroupIcon />
                                            </IconButton>
                                        </Tooltip>
                                    </Box>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <Dialog open={openInvite} onClose={handleCloseInvite} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Agregar staff</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Ingresa el correo electrónico del usuario que quieres invitar como staff (debe estar registrado con ese correo)
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="email"
                        onChange={changeEmail}
                        value={email}
                        label="Correo electrónico"
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleInvite} color="primary">
                        Invitar
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog open={openRoles} onClose={handleCloseRoles} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Asignar rol</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Seleccione el rol que tendrá este usuario en el evento.
                    </DialogContentText>
                    <FormControl variant="outlined" className={classes.formControl}>
                        <InputLabel id="equipo-select">Rol</InputLabel>
                        <Select
                            labelId="demo-simple-select-outlined-label"
                            id="demo-simple-select-outlined"
                            value={idrole}
                            onChange={handleChangeRole}
                            label="Rol"
                        >
                            <MenuItem value={0}>
                                <em>Seleccionar rol</em>
                            </MenuItem>
                            {(roles.map((t) => {
                                return (
                                    <MenuItem key={t.idrole} value={t.idrole}>{t.name}</MenuItem>
                                )
                            }))}
                        </Select>
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleAssignRole} color="primary" disabled={idrole === 0}>
                        Asignar rol
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog open={openTeams} onClose={() => setOpenTeams(false)} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Asignar a equipo</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Seleccione el equipo que tendra asignado este mentor.
                    </DialogContentText>
                    <FormControl variant="outlined" className={classes.formControl}>
                        <InputLabel id="equipo-select">Equipo</InputLabel>
                        <Select
                            value={idgroup}
                            onChange={handleChangeTeam}
                            label="Equipo"
                        >
                            <MenuItem value={0}>
                                <em>Seleccionar equipo</em>
                            </MenuItem>
                            {(teams.map((t) => {
                                return (
                                    <MenuItem key={t.idgroup} value={t.idgroup}>{t.name}</MenuItem>
                                )
                            }))}
                        </Select>
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleAssignTeam} color="primary" disabled={idgroup === 0}>
                        Aceptar
                    </Button>
                </DialogActions>
            </Dialog>
            <Snackbar open={openSnack} autoHideDuration={3000} onClose={() => setOpenSnack(false)}>
                <Alert onClose={() => setOpenSnack(false)} severity={severitySnack}>
                    {messageSnack}
                </Alert>
            </Snackbar>
        </>
    );
}

export default Staff;