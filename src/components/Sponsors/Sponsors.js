import React, { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import EditIcon from '@material-ui/icons/Edit';
import InfoIcon from '@material-ui/icons/Info';
import DeleteIcon from '@material-ui/icons/Delete';
import { Auth } from 'aws-amplify';
import { SponsorCreate, SponsorUpdate, SponsorList, SponsorDelete } from '../../api/services';

const Sponsors = (props) => {
    const event = props.history.location.state.event;
    const [sponsors, setSponsors] = useState([]);
    const [openCreate, setOpenCreate] = useState(false);
    const [name, setName] = useState('');
    const [business, setBusiness] = useState('');
    const [contact, setContact] = useState('');
    const [phone, setPhone] = useState('');
    const [comment, setComment] = useState('');
    const [web, setWeb] = useState('');
    const [editing, setEditing] = useState(false);
    const [disabled, setDisabled] = useState(false);
    const [current, setCurrent] = useState(null);

    const getSponsors = async () => {
        let t = await SponsorList({ idevent: event.idevent })
        setSponsors(t.body)
    }

    const handleClickDelete = async (g, index) => {
        let aux = JSON.parse(JSON.stringify(sponsors));
        aux.splice(index, 1);
        setSponsors(aux);
        let t = await SponsorDelete({ idsponsor: g.idsponsor })
        if (t.body.success) getSponsors();
    }

    const handleCreate = async () => {
        if (disabled) {
            setOpenCreate(false);
            return;
        }
        let user = await Auth.currentUserInfo();
        if (editing) {
            let data = {
                idsponsor: current,
                idevent: event.idevent,
                name: name,
                business: business,
                contact: contact,
                contact_phone: phone,
                comment: comment,
                web: web,
                iduser: Number(user.attributes["custom:iduser"])
            }
            await SponsorUpdate(data);
        } else {
            let data = {
                idevent: event.idevent,
                name: name,
                business: business,
                contact: contact,
                contact_phone: phone,
                comment: comment,
                web: web,
                iduser: Number(user.attributes["custom:iduser"])
            }
            await SponsorCreate(data);
        }
        getSponsors();
        setOpenCreate(false);
    }

    const handleClickNew = (g) => {
        if (g === null) {
            setName('');
            setBusiness('');
            setContact('');
            setPhone('');
            setComment('');
            setWeb('');
            setEditing(false);
            setDisabled(false);
        } else {
            setName(g.name);
            setBusiness(g.business);
            setContact(g.contact);
            setPhone(g.contact_phone);
            setComment(g.comment);
            setWeb(g.web);
        }
        setOpenCreate(true);
    }

    useEffect(() => { getSponsors() }, []);

    return (
        <>
            <Typography variant="h6" gutterBottom>
                Sponsors
            </Typography>
            <Button variant="contained" color="primary" onClick={() => handleClickNew(null)} style={{ marginBottom: '1rem' }}>Registrar sponsor</Button>
            {(!Array.isArray(sponsors) || !sponsors.length) ? <Typography variant="body1">No hay sponsors registrados.</Typography> :
                <TableContainer component={Paper}>
                    <Table aria-label="spanning table" size={'small'}>
                        <TableHead>
                            <TableRow>
                                <TableCell>Nombre</TableCell>
                                <TableCell align="right">Rubro</TableCell>
                                <TableCell align="right">Contacto</TableCell>
                                <TableCell align="right">Teléfono</TableCell>
                                <TableCell align="right">Acciones</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {sponsors.map((p, index) => (
                                <TableRow key={p.idsponsor}>
                                    <TableCell>{p.name}</TableCell>
                                    <TableCell align="right">{p.business}</TableCell>
                                    <TableCell align="right">{p.contact}</TableCell>
                                    <TableCell align="right">{p.contact_phone}</TableCell>
                                    <TableCell align="right">
                                        <IconButton size="small" onClick={() => { handleClickNew(p); setEditing(false); setDisabled(true); }}>
                                            <InfoIcon />
                                        </IconButton>
                                        <IconButton size="small" onClick={() => { handleClickNew(p); setEditing(true); setDisabled(false); setCurrent(p.idsponsor); }}>
                                            <EditIcon />
                                        </IconButton>
                                        <IconButton size="small" onClick={() => handleClickDelete(p, index)}>
                                            <DeleteIcon />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>}
            <Dialog open={openCreate} onClose={() => setOpenCreate(false)} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Agregar sponsor</DialogTitle>
                <DialogContent>
                    <Grid container spacing={1}>
                        <Grid item sm={12}>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                fullWidth
                                label="Nombre"
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                                InputLabelProps={{ shrink: true }}
                                disabled={disabled}
                            />
                        </Grid>
                        <Grid item sm={12}>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                fullWidth
                                label="Rubro comercial"
                                value={business}
                                onChange={(e) => setBusiness(e.target.value)}
                                InputLabelProps={{ shrink: true }}
                                disabled={disabled}
                            />
                        </Grid>
                        <Grid item sm={12}>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                fullWidth
                                label="Contacto"
                                value={contact}
                                onChange={(e) => setContact(e.target.value)}
                                InputLabelProps={{ shrink: true }}
                                disabled={disabled}
                            />
                        </Grid>
                        <Grid item sm={12}>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                fullWidth
                                label="Teléfono contacto"
                                value={phone}
                                onChange={(e) => setPhone(e.target.value)}
                                InputLabelProps={{ shrink: true }}
                                disabled={disabled}
                            />
                        </Grid>
                        <Grid item sm={12}>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                fullWidth
                                label="Sitio Web"
                                value={web}
                                onChange={(e) => setWeb(e.target.value)}
                                InputLabelProps={{ shrink: true }}
                                disabled={disabled}
                            />
                        </Grid>
                        <Grid item sm={12}>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                fullWidth
                                label="Información adicional"
                                value={comment}
                                onChange={(e) => setComment(e.target.value)}
                                InputLabelProps={{ shrink: true }}
                                disabled={disabled}
                                multiline
                            />
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCreate} color="primary">
                        Aceptar
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
}

export default Sponsors;