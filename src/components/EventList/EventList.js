import React from 'react';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from "react-router-dom";

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
    },
    media: {
        height: '5rem',
    },
});

const EventList = (props) => {
    const { title, events } = props;
    const classes = useStyles();
    const history = useHistory();

    const handleClickEvent = (event) => {
        history.push("/event/" + String(event.idevent), event);
    }

    return (
        <Grid container spacing={2} style={{ margin: '1rem' }}>
            <Grid item sm={12}>
                <Typography variant="h4" gutterBottom>
                    {title}
                </Typography>
            </Grid>
            {(events.map((event) => {
                return (
                    <Grid key={event.idevent} item sm={3}>
                        <Card className={classes.root} onClick={() => handleClickEvent(event)} style={{ height: '350px' }}>
                            <CardActionArea style={{ height: '350px' }} >
                                <CardMedia
                                    title={event.name}
                                    component={() => (
                                        <img
                                            alt={event.name}
                                            src={"data:image;base64," + event.img}
                                            height="200"
                                            width="350"
                                        />
                                    )}>
                                </CardMedia>
                                <CardContent>
                                    <Typography gutterBottom variant="h5" component="h2">
                                        {event.name}
                                    </Typography>
                                    <Typography variant="body2" color="textSecondary" component="p">
                                        {event.short_desc}
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Card>
                    </Grid>)
            }))}
        </Grid>
    );
}

export default EventList;