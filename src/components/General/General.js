import React, { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { EventStartEvent, EventUpdate, EventRead } from '../../api/services';
import { Button, Typography } from '@material-ui/core';
import { Auth } from 'aws-amplify';

const General = (props) => {
    const event = props.history.location.state.event;
    const [openStart, setOpenStart] = useState(false);
    const [disabledUpdate, setDisabledUpdate] = useState(true);
    const [input, setInput] = useState({
        name: '',
        shortDesc: '',
        longDesc: '',
        location: '',
        video: ''
    });

    const handleClickStart = async () => {
        await EventStartEvent({ idevent: event.idevent })
        setOpenStart(false)
    }

    const getEventInfo = async () => {
        let t = await EventRead({ idevent: event.idevent });
        let aux = {
            name: t.body.name,
            shortDesc: t.body.short_desc,
            longDesc: t.body.long_desc,
            location: t.body.location,
            video: t.body.videolink,
        }
        setInput(aux);
    }

    const handleClickUpdate = async () => {
        let user = await Auth.currentUserInfo();
        let today = new Date()
        let data = {
            idevent: event.idevent,
            name: input.name,
            short_desc: input.shortDesc,
            long_desc: input.longDesc,
            location: input.location,
            videolink: input.video,
            date: today,
            iduser: Number(user.attributes["custom:iduser"])
        }
        let t = await EventUpdate(data);
        setDisabledUpdate(true);
    }

    const handleInputChange = (event) => {
        setInput({ ...input, [event.target.id]: event.target.value });
        setDisabledUpdate(false);
    }

    useEffect(() => {
        getEventInfo();
    }, []);


    return (
        <>
            <Typography variant="h6" style={{ display: 'inline-block' }}>
                Información del evento
            </Typography>
            <Button color='primary' variant="contained" onClick={handleClickUpdate} disabled={disabledUpdate} style={{ marginLeft: '1rem' }}>Guardar cambios</Button>
            <Grid container spacing={1}>
                <Grid item sm={12}>
                    <TextField
                        InputLabelProps={{ shrink: true }}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="name"
                        name="name"
                        label="Nombre del evento"
                        value={input.name}
                        onChange={handleInputChange}
                    />
                </Grid>
                <Grid item sm={12}>
                    <TextField
                        multiline
                        InputLabelProps={{ shrink: true }}
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        name="shortDesc"
                        label="Descripción breve"
                        id="shortDesc"
                        value={input.shortDesc}
                        onChange={handleInputChange}
                        inputProps={{ maxLength: 200 }}
                    />
                </Grid>
                <Grid item sm={12}>
                    <TextField
                        multiline
                        rows={3}
                        rowsMax={10}
                        InputLabelProps={{ shrink: true }}
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        name="longDesc"
                        label="Información general"
                        id="longDesc"
                        value={input.longDesc}
                        onChange={handleInputChange}
                        inputProps={{ maxLength: 10000 }}
                    />
                </Grid>
                <Grid item sm={12}>
                    <TextField
                        InputLabelProps={{ shrink: true }}
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        name="video"
                        label="Video promocional (Enlace de YouTube)"
                        helperText="Ejemplos: https://www.youtube.com/watch?v=wJWksPWDKOc ó https://youtu.be/wJWksPWDKOc"
                        id="video"
                        value={input.video}
                        onChange={handleInputChange}
                        inputProps={{ maxLength: 200 }}
                    />
                </Grid>
                <Grid item sm={12}>
                    <TextField
                        InputLabelProps={{ shrink: true }}
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        name="location"
                        label="Ubicación"
                        id="location"
                        value={input.location}
                        onChange={handleInputChange}
                        inputProps={{ maxLength: 300 }}
                    />
                </Grid>
            </Grid>
            <Typography variant="h6" gutterBottom>
                Iniciar evento principal
            </Typography>
            <Typography variant="body1">
                Al iniciar el evento, todos los equipos aceptados (ver inscripciones) podrán ingresar acceder al menú de participantes,
                que les permite interactuar con asesores y mentores, ver los anuncions globales, subir sus archivos para ser corregidos y realizar feedback sobre el evento.
                Esta acción es irreversible y al ejecutarse ya no se podrán aceptar más equipos participantes ni modificar los criterios de evaluación de evaluación.
            </Typography>
            <Button color='secondary' variant="contained" onClick={handleClickStart}>Iniciar evento</Button>
        </>
    );
}

export default General;