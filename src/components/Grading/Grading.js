import React, { useEffect, useState } from 'react';
import { CriteriaList, CriteriaCreate, CriteriaDelete } from '../../api/services';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import { Auth } from 'aws-amplify';
import { useStyles } from './Grading.module'
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';

const Grading = (props) => {
    const event = props.history.location.state.event;
    const classes = useStyles();
    const [criteria, setCriteria] = useState([]);
    const [name, setName] = useState('');
    const [info, setInfo] = useState('');
    const [min, setMin] = useState('');
    const [max, setMax] = useState('');
    const [weight, setWeight] = useState('1');
    const [openCreate, setOpenCreate] = useState(false);
    const [openSnack, setOpenSnack] = useState(false);
    const [severitySnack, setSeveritySnack] = useState('success');
    const [messageSnack, setMessageSnack] = useState('');

    const getCriteria = async () => {
        let t = await CriteriaList({ idevent: event.idevent })
        setCriteria(t.body)
    }

    const popSnack = (msg, sev) => {
        setMessageSnack(msg)
        setSeveritySnack(sev)
        setOpenSnack(true)
    }

    const handleCreateNewCriteria = async () => {
        let user = await Auth.currentUserInfo();
        let data = {
            idevent: event.idevent,
            name: name,
            info: info,
            min: Number(min),
            max: Number(max),
            weight: Number(weight),
            iduser: Number(user.attributes["custom:iduser"])
        }
        await CriteriaCreate(data);
        setCriteria([]);
        getCriteria();
        setOpenCreate(false);
    }

    const handleClickCriteria = () => {
        setName('');
        setInfo('');
        setMax('');
        setMin('');
        setWeight('1');
        setOpenCreate(true);
    }

    const handleClickDelete = async (g, index) => {
        if (event.status >= 4) {
            popSnack('No se puede eliminar un criterio cuando el evento ya empezó', 'error');
            return;
        }
        let aux = JSON.parse(JSON.stringify(criteria));
        aux.splice(index, 1);
        setCriteria(aux)
        let t = await CriteriaDelete({ idevent: event.idevent, idcriteria: g.idcriteria })
        if (t.body.success) popSnack('Criterio eliminado', 'success');
    }

    const handleCloseCreate = () => setOpenCreate(false);
    const changeName = (event) => setName(event.target.value);
    const changeInfo = (event) => setInfo(event.target.value);
    const changeMin = (event) => setMin(event.target.value);
    const changeMax = (event) => setMax(event.target.value);
    const changeWeight = (event) => setWeight(event.target.value);

    useEffect(() => {
        getCriteria();
    }, []);

    return (
        <>
            <Typography variant="h6" gutterBottom>
                Criterios de calificación
            </Typography>
            <Button variant="contained" color="primary" onClick={handleClickCriteria} style={{ marginBottom: '1rem' }}>Agregar criterio</Button>
            {criteria.map((c, index) => (
                <Card className={classes.root}>
                    <CardHeader
                        title={'C' + (index + 1) + '. ' + c.name}
                        subheader={'Puntaje mínimo: ' + c.min + ' | Puntaje máximo: ' + c.max + ' | Peso: ' + c.weight}
                        action={
                            <IconButton aria-label="settings" onClick={() => handleClickDelete(c, index)}>
                                <DeleteOutlineIcon />
                            </IconButton>
                        }
                    />
                    <CardContent style={{ paddingTop: '0px' }}>
                        <Typography variant="body2" component="p">
                            {c.info}
                        </Typography>
                    </CardContent>
                </Card>
            ))}
            <Dialog open={openCreate} onClose={handleCloseCreate} aria-labelledby="form-dialog-title" fullWidth maxWidth='sm'>
                <DialogTitle id="form-dialog-title">Agregar criterio</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Ingrese los siguientes datos del nuevo criterio a evaluar en la rúbrica de calificación
                    </DialogContentText>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                margin="dense"
                                id="name"
                                onChange={changeName}
                                value={name}
                                label="Criterio"
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                margin="dense"
                                id="info"
                                onChange={changeInfo}
                                value={info}
                                label="Descripción"
                                fullWidth
                                multiline
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                margin="dense"
                                id="info"
                                onChange={changeMin}
                                value={min}
                                label="Puntaje mínimo"
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                margin="dense"
                                id="info"
                                onChange={changeMax}
                                value={max}
                                label="Puntaje máximo"
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                margin="dense"
                                id="info"
                                onChange={changeWeight}
                                value={weight}
                                label="Peso"
                                fullWidth
                            />
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <Button color="primary" onClick={handleCreateNewCriteria}>
                        Aceptar
                    </Button>
                </DialogActions>
            </Dialog>
            <Snackbar open={openSnack} autoHideDuration={3000} onClose={() => setOpenSnack(false)}>
                <Alert onClose={() => setOpenSnack(false)} severity={severitySnack}>
                    {messageSnack}
                </Alert>
            </Snackbar>
        </>
    );
}

export default Grading;