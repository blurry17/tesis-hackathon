import React, { useEffect, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import { Auth } from 'aws-amplify';
import { TaskList, TaskCreate, TaskUpdate, PurchaseCreate, PurchaseList, TaskDelete, PurchaseDelete } from '../../api/services';
import { Divider } from '@material-ui/core';
import ClearIcon from '@material-ui/icons/Clear';

const Tasks = (props) => {
    const event = props.history.location.state.event;
    const [tasks, setTasks] = useState([]);
    const [purchases, setPurchases] = useState([]);
    const [openCreateTask, setOpenCreateTask] = useState(false);
    const [openCreatePurchase, setOpenCreatePurchase] = useState(false);
    const [name, setName] = useState('');
    const [info, setInfo] = useState('');
    const [price, setPrice] = useState('');
    const [total, setTotal] = useState(0);

    const getTasks = async () => {
        let t = await TaskList({ idevent: event.idevent })
        setTasks(t.body)
    }

    const getPurchases = async () => {
        let t = await PurchaseList({ idevent: event.idevent })
        setPurchases(t.body)
        let aux = 0;
        for (let i = 0; i < t.body.length; i++) aux = aux + t.body[i].price
        setTotal(aux);
    }

    const handleCheckTask = async (t, index) => {
        let clone = Array.from(tasks)
        clone[index].status = 1 - clone[index].status;
        setTasks(clone)
        await TaskUpdate({ idtask: t.idtask, status: clone[index].status })
    }

    const handleCreateTask = async () => {
        let user = await Auth.currentUserInfo();
        await TaskCreate({ idevent: event.idevent, name: name, iduser: Number(user.attributes["custom:iduser"]) })
        setTasks([])
        getTasks()
        setOpenCreateTask(false);
    }

    const handleCreatePurchase = async () => {
        let user = await Auth.currentUserInfo();
        await PurchaseCreate({ idevent: event.idevent, name: name, info: info, price: Number(price.replace(",", ".")), iduser: Number(user.attributes["custom:iduser"]) })
        setPurchases([])
        setTotal(0)
        getPurchases()
        setOpenCreatePurchase(false);
    }

    const handleClickDeleteTask = async (g, index) => {
        let aux = JSON.parse(JSON.stringify(tasks));
        aux.splice(index, 1);
        setTasks(aux)
        await TaskDelete({ idtask: g.idtask })
    };

    const handleClickDeletePurchase = async (g, index) => {
        let aux = JSON.parse(JSON.stringify(purchases));
        aux.splice(index, 1);
        setPurchases(aux)
        let temp = 0;
        for (let i = 0; i < aux.length; i++) temp = temp + aux[i].price
        setTotal(temp);
        await PurchaseDelete({ idpurchase: g.idpurchase })
    };

    const handleClickNewTask = () => { setOpenCreateTask(true); setName(''); }
    const handleClickNewPurchase = () => { setOpenCreatePurchase(true); setName(''); setInfo(''); setPrice(''); }
    const handleCloseCreateTask = () => setOpenCreateTask(false);
    const handleCloseCreatePurchase = () => setOpenCreatePurchase(false);

    const changeName = (event) => setName(event.target.value);
    const changeInfo = (event) => setInfo(event.target.value);
    const changePrice = (event) => setPrice(event.target.value);

    useEffect(() => {
        getTasks();
        getPurchases();
    }, []);


    return (
        <>
            <Typography variant="h6" gutterBottom style={{ marginTop: '1rem', display: 'inline' }}>
                Lista de tareas por hacer
            </Typography>
            <Button variant="contained" color="primary" onClick={handleClickNewTask} style={{ marginLeft: '1rem' }}>Nueva tarea</Button>
            {(!Array.isArray(tasks) || !tasks.length) ? <Typography variant="body1">No hay tareas registradas.</Typography> : <></>}
            <Paper style={{ marginTop: '1rem' }}>
                {tasks.map((t, index) => {
                    return (
                        <>
                            <IconButton style={{ display: 'inline' }} size="small" onClick={() => handleClickDeleteTask(t, index)}>
                                <ClearIcon />
                            </IconButton>
                            <FormControlLabel
                                control={<Checkbox color="primary" checked={t.status === 1} onClick={() => handleCheckTask(t, index)} />}
                                label={t.name}
                                style={t.status === 1 ? { textDecoration: 'line-through', display: 'inline-block' } : { display: 'inline-block' }}
                            />
                            <br></br>
                        </>
                    )
                })}
            </Paper>
            <Divider style={{ marginBottom: '1rem', marginTop: '1rem' }}></Divider>
            <Typography variant="h6" gutterBottom style={{ paddingTop: '1rem', display: 'inline' }}>
                Lista de compras y servicios
            </Typography>
            <Button variant="contained" color="primary" onClick={handleClickNewPurchase} style={{ marginLeft: '1rem' }}>Nueva compra</Button>
            <TableContainer component={Paper} style={{ marginTop: '1rem' }}>
                <Table size='small'>
                    <TableHead>
                        <TableRow>
                            <TableCell>Compra/Servicio</TableCell>
                            <TableCell>Descripción</TableCell>
                            <TableCell align="right">Costo (S/.)</TableCell>
                            <TableCell align="right">Acciones</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {purchases.map((p, index) => (
                            <TableRow key={p.idpurchase}>
                                <TableCell>{p.name}</TableCell>
                                <TableCell>{p.info}</TableCell>
                                <TableCell align="right">{p.price.toFixed(2)}</TableCell>
                                <TableCell align="right">
                                    <IconButton style={{ display: 'inline' }} size="small" onClick={() => handleClickDeletePurchase(p, index)}>
                                        <DeleteOutlineIcon />
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        ))}
                        <TableRow>
                            <TableCell><b>Total:</b></TableCell>
                            <TableCell></TableCell>
                            <TableCell align="right">{total.toFixed(2)}</TableCell>
                            <TableCell></TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </TableContainer>
            <Dialog open={openCreateTask} onClose={handleCloseCreateTask} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Agregar tarea</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Ingrese el nombre de la tarea
                    </DialogContentText>
                    <TextField
                        margin="dense"
                        id="name"
                        onChange={changeName}
                        value={name}
                        label="Tarea"
                        fullWidth
                        multiline
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCreateTask} color="primary">
                        Aceptar
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog open={openCreatePurchase} onClose={handleCloseCreatePurchase} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Agregar compra</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Ingrese el producto o servicio
                    </DialogContentText>
                    <TextField
                        margin="dense"
                        id="name"
                        onChange={changeName}
                        value={name}
                        label="Producto/Servicio"
                        fullWidth
                    />
                    <TextField
                        margin="dense"
                        id="info"
                        onChange={changeInfo}
                        value={info}
                        label="Descripción"
                        fullWidth
                        multiline
                    />
                    <TextField
                        margin="dense"
                        id="price"
                        onChange={changePrice}
                        value={price}
                        label="Precio en SOLES"
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCreatePurchase} color="primary">
                        Aceptar
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
}

export default Tasks;