import React from 'react';
import MyEvents from '../components/MyEvents/MyEvents';

const MyEventsContainer = () => {
    return (
        <MyEvents />
    );
}

export default MyEventsContainer;