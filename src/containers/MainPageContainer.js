import React, { useState, useEffect } from 'react';
import EventList from '../components/EventList/EventList';
import { EventListRecent } from '../api/services';

const MainPageContainer = () => {
    const [recentEvents, setRecentEvents] = useState([]);

    useEffect(() => {
        async function getRecentEvents() {
            let data = await EventListRecent(null);
            setRecentEvents(data.body)
        }
        getRecentEvents();
    }, []);

    return (
        <>
            <EventList mode={'i'} title={"Recién anunciados"} events={recentEvents} />
        </>
    );
}

export default MainPageContainer;