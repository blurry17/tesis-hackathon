import React from 'react';
import TopBar from '../components/TopBar/TopBar';

const TopBarContainer = () => {
    return (
        <TopBar />
    );
}

export default TopBarContainer;