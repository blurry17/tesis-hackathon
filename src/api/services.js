import axios from "axios";
import config from '../config.json'

const getAPIBack = () => {
    var apiBack = config.backend.API;
    return apiBack;
};

const apiWithFiles = axios.create({
    baseURL: "http://" + getAPIBack(),
    headers: {
        "Content-Type": "multipart/form-data",
        Authorization: localStorage.getItem("accessToken"),
    },
});

const api = axios.create({
    baseURL: "http://" + getAPIBack(),
    headers: {
        "X-Requested-With": "XMLHttpRequest",
        Authorization: localStorage.getItem("accessToken"),
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "POST,GET",
        Accept: "*/*",
    },
});

export const EventGetByUser = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/getEventsByUser",
        data: body,
    });
    //console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const UserCreate = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/user/create",
        data: body,
    });
    //console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const UserGetBasic = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/user/getBasic",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventCreate = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/create",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventUploadFiles = async (body, files) => {
    var formData = new FormData();
    //files.forEach((file) => formData.append("file[]", file));
    formData.append("file[]", files[0])
    formData.append("file[]", files[1])
    formData.append("data", JSON.stringify(body));
    const response = await apiWithFiles({
        method: "post",
        url: "/api/event/uploadFiles",
        data: formData,
    });
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventListRecent = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/listRecent",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const TeamsGetCreated = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/team/listCreated",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const TeamsGetCreatedSimple = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/team/listCreatedSimple",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const TeamsGetOther = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/team/listOther",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const TeamsCreate = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/team/create",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const NotificationJoinTeam = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/notification/joinTeam",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const NotificationGet = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/notification/get",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const NotificationAcceptTeam = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/notification/acceptTeam",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventEnrollTeam = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/enrollTeam",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventGetEnrolledG = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/getEnrolledG",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventGetEnrolledI = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/getEnrolledI",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventGetAccepted = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/getAccepted",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventDenyTeam = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/denyTeam",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventAcceptTeam = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/acceptTeam",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventEnrollUser = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/enrollUser",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const TeamCreateTemp = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/team/createTemp",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventGetParticipating = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/getParticipating",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const NotificationJoinEvent = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/notification/joinEvent",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const StaffList = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/staff/list",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const NotificationAcceptEvent = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/notification/acceptEvent",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const StaffListRoles = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/staff/listRoles",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const StaffAssignRole = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/staff/assignRole",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const ActivityList = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/activity/list",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const ActivityCreate = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/activity/create",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const ActivityUpdateList = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/activity/updateList",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const UserGetInfo = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/user/getInfo",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const TeamGetMembers = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/team/getMembers",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const TaskCreate = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/task/create",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const TaskList = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/task/list",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const TaskUpdate = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/task/update",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const PurchaseCreate = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/purchase/create",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const PurchaseList = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/purchase/list",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const CriteriaList = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/criteria/list",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const CriteriaCreate = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/criteria/create",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventGetTeamId = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/getTeamId",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const WorkListByGroup = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/work/listByGroup",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const WorkUpload = async (body, files) => {
    var formData = new FormData();
    for (let i = 0; i < files.length; i++) formData.append("file[]", files[i])
    formData.append("data", JSON.stringify(body));
    const response = await apiWithFiles({
        method: "post",
        url: "/api/work/upload",
        data: formData,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventGetRoleId = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/getRoleId",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventGetActive = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/getActive",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventStartEvent = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/startEvent",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const CriteriaGetJudgeScore = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/criteria/getJudgeScore",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const CriteriaSaveScore = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/criteria/saveScore",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventListNews = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/listNews",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventAddNews = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/addNews",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventGetMessages = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/getMessages",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventSendMessage = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/sendMessage",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventGetQuestions = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/getQuestions",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventSendQuestion = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/sendQuestion",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const StaffAssignTeam = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/staff/assignTeam",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventGetTeamMentoring = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/getTeamMentoring",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const CriteriaGetAllScores = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/criteria/getAllScores",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventSendFeedback = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/sendFeedback",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const ReportEnrolledAccepted = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/report/enrolledAccepted",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const ReportCollegeDiversity = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/report/collegeDiversity",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const ReportSexDiversity = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/report/sexDiversity",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const ReportMajorDiversity = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/report/majorDiversity",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const ReportStudyLevelDiversity = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/report/studyLevelDiversity",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const ReportRatings = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/report/ratings",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const ReportComments = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/report/comments",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventGetEvidence = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/getEvidence",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventUploadEvidence = async (body, files) => {
    var formData = new FormData();
    for (let i = 0; i < files.length; i++) formData.append("file[]", files[i])
    formData.append("data", JSON.stringify(body));
    const response = await apiWithFiles({
        method: "post",
        url: "/api/event/uploadEvidence",
        data: formData,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
}

export const EventUploadRules = async (body, file) => {
    var formData = new FormData();
    formData.append("file", file)
    formData.append("data", JSON.stringify(body));
    const response = await apiWithFiles({
        method: "post",
        url: "/api/event/uploadRules",
        data: formData,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
}

export const EventUploadImage = async (body, file) => {
    var formData = new FormData();
    formData.append("file", file)
    formData.append("data", JSON.stringify(body));
    const response = await apiWithFiles({
        method: "post",
        url: "/api/event/uploadImage",
        data: formData,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
}

export const EventDownloadRules = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/downloadRules",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventUpdateRepository = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/updateRepository",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventGetRepository = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/getRepository",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const UserGetSchools = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/user/getSchools",
        data: body,
    });
    //console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const UserGetMajors = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/user/getMajors",
        data: body,
    });
    //console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const UserGetGenders = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/user/getGenders",
        data: body,
    });
    //console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const UserGetLevels = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/user/getLevels",
        data: body,
    });
    //console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const UserGetDiets = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/user/getDiets",
        data: body,
    });
    //console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const UserEdit = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/user/edit",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const SponsorCreate = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/sponsor/create",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const SponsorUpdate = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/sponsor/update",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const SponsorList = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/sponsor/list",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const TeamDelete = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/team/delete",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const NotificationDelete = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/notification/delete",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const ActivityDelete = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/activity/delete",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const TaskDelete = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/task/delete",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const PurchaseDelete = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/purchase/delete",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const CriteriaDelete = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/criteria/delete",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const WorkDelete = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/work/delete",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const SponsorDelete = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/sponsor/delete",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventFinishHacking = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/finishHacking",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const TeamGetScores = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/team/getScores",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventUpdate = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/update",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};

export const EventRead = async (body) => {
    const response = await api({
        method: "post",
        url: "/api/event/read",
        data: body,
    });
    console.log("RESPUESTA API: ", response);
    if (response.status >= 200 && response.status < 300) return response.data;
    else return null;
};